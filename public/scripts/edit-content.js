/* eslint-disable prettier/prettier */
const editLabel         = document.querySelectorAll(".edit-label-bio");
const editTextArea      = document.querySelectorAll(".custom-textarea");
const editSubtitle      = document.querySelectorAll(".card-add-post__card-subtitle-edit")[0];
const currentPassword   = document.querySelector(".currentPassword");
const password          = document.querySelector(".password");
const confirmPassword   = document.querySelector(".confirm-password");
const firstName         = document.querySelector(".firstName");
const lastName          = document.querySelector(".lastName");
const email             = document.querySelector(".email");
const userName          = document.querySelector(".userName");
const bio_div           = document.querySelector(".edit-bio");
const first_submit      = document.querySelector(".edit-content-form1");
const second_submit     = document.querySelector(".edit-content-form2");
let fildsError = {};
let fildsErrorPass = {};


const regex = RegExp("^\\s*$");
let counter = 0;

editTextArea.forEach(text => {
  text.addEventListener("focus", () => {
    if (regex.test(text.value)) {
      text.value = "";
    }
  });

  text.addEventListener("keyup", event => {
    editLabel[0].style = "opacity: 0;";
    let iterAndMax = editSubtitle.innerHTML.split('/');
    iterAndMax[0] = text.value.length;
    if (iterAndMax[0] > iterAndMax[1]) {
      editSubtitle.style = "color : red!important;"
      bio_div.style = "border-bottom: 1px solid red";
      delete fildsError.bio;
    }
    else {
      editSubtitle.style = "color : #6c757d!important;";
      bio_div.style = "border-bottom: 1px solid silver";
      fildsError["bio"] = true;
    }
    editSubtitle.innerHTML = "" + iterAndMax[0] + "/" + iterAndMax[1];
  });

  text.addEventListener("blur", () => {
    if (!text.value || regex.test(text.value)) {
      editLabel[0].style = "opacity: 1;";
      text.rows = 1;
      text.value = "";
    }
  });

  text.addEventListener("scroll", () => {
    counter++;
    if (counter === 2) {
      counter = 0;
      return;
    }
    text.rows += 1;
  });
});

firstName.addEventListener("keyup", function(e) {
  let a = e.target.value;
  if (!regExpName(a)) {
    e.target.style = "border-bottom: 1px solid red";
    delete fildsError.firstName;
  } else if (regExpName(a)) {
    e.target.style = "border-bottom: 1px solid green";
    e.target.innerHTML = "";
    fildsError["firstName"] = true;
  }
});

lastName.addEventListener("keyup", function(e) {
  let a = e.target.value;
  if (!regExpName(a)) {
    e.target.style = "border-bottom: 1px solid red";
    delete fildsError.lastName;
  } else if (regExpName(a)) {
    e.target.style = "border-bottom: 1px solid green";
    e.target.innerHTML = "";
    fildsError["lastName"] = true;
  }
});

email.addEventListener("keyup", function(e) {
  let a = e.target.value;
  if (!regExpEmail(a)) {
    e.target.style = "border-bottom: 1px solid red";
    delete fildsError.email;
  } else if (regExpEmail(a)) {
    e.target.style = "border-bottom: 1px solid green";
    e.target.innerHTML = "";
    fildsError["email"] = true;
  }
});

userName.addEventListener("keyup", function(e) {
  let a = e.target.value;
  if (!regExpUser(a)) {
    e.target.style = "border-bottom: 1px solid red";
    delete fildsError.username;
  } else if (regExpUser(a)) {
    e.target.style = "border-bottom: 1px solid green";
    e.target.innerHTML = "";
    fildsError["username"] = true;
  }
});

password.addEventListener("keyup", event => {
  let reg = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/;
  if (!reg.test(event.target.value)) {
    event.target.style = "border-bottom: 1px solid red";
    delete fildsErrorPass.password;
  } else if (reg.test(event.target.value)) {
    event.target.innerHTML = "";
    event.target.style = "border-bottom: 1px solid green";
    fildsErrorPass["password"] = true;
  }
});

confirmPassword.addEventListener("keyup", event => {
  event.target.style = "border-bottom: 1px solid green";
  fildsErrorPass["password"] = true;
  if (
      password.value !== confirmPassword.value ||
      password.value === '' ||
      confirmPassword.value === ''
  ) {
    event.target.style = "border-bottom: 1px solid red";
    delete fildsErrorPass.password;
  }
});

password.addEventListener("change", event => {
  if (password.value !== confirmPassword.value) {
    confirmPassword.style = "border-bottom: 1px solid red";
    delete fildsErrorPass.password;
  }
  else if (confirmPassword.value && password.value){
    confirmPassword.style = "border-bottom: 1px solid green";
    fildsErrorPass["password"] = true;
  }
})
function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}

first_submit.addEventListener("submit" , event => {
  if (isEmpty(fildsError)) {
    event.preventDefault();
    return;
  }
  for (let key in fildsError) {
    if (fildsError[key] === false) {
      event.preventDefault();
    }
  }
});

second_submit.addEventListener("submit" , event => {
  if (currentPassword.value) {
    for (let key in fildsErrorPass) {
      if (fildsErrorPass[key] === false) {
        event.preventDefault();
      }
    }
  }
  else if (!password.value && !confirmPassword.value){
    event.preventDefault();
  }
  else {
    currentPassword.style = "border-bottom: 1px solid red";
    event.preventDefault();
  }
});

function regExpName(st) {
  return /^([a-zA-zа-яА-я]{1,30})$/.test(st);
}

//User Name
function regExpUser(st) {
  return /^([a-zA-z0-9]{3,15})$/.test(st);
}

// email
function regExpEmail(st) {
  return /^[a-zA-z]+\d*[a-zA-z]+\d*@[a-z]+\.[a-z]{2,5}$/.test(st);
}
