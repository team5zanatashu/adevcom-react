const searchInputField = document.querySelectorAll('.search-if');
const searchButton = document.querySelectorAll('.custom-search-logo');
let searchListArr = [];
let searchList = document.querySelector('.header-search-list');
const lsData = JSON.parse(localStorage.getItem('search-data'));
let regExpSearchTrue = /(\w*\d*)/gi;

searchButton.forEach(function(elem) {
  elem.addEventListener('click', function() {
    if (searchInputField[0].value === '') {
      searchInputField[0].placeholder = 'Type login';
      window.location.href = '../search-page.html';
    } else {
      if(regExpSearchTrue.test(searchInputField[0].value)) {
        searchInputField[0].placeholder = 'Search';
        console.log('Search button');
        let searchValue = searchInputField[0].value;
        console.log(searchValue);
        searchListArr.unshift(searchValue);
        console.log(searchListArr);
        setToLocal();
        console.log('Local has been set');
        console.log('LS DATA', lsData);
        searchInputField[0].value = '';
        window.location.href = '../search-page.html';
      }
    }
  });
});


window.onload = function() {
  if (lsData !== null) {
    searchListArr = lsData;
  }
};

searchInputField.forEach(function(elem) {
  elem.addEventListener('click', function() {
    while (searchList.firstChild) {
      searchList.removeChild(searchList.firstChild);
    }
    if (searchList.classList.contains('header-search-list')) {
      searchList.classList.add('search-list-show');
      for (let i = 0; i < searchListArr.length; i++) {
        const rowLine = document.createElement('li');
        rowLine.className = 'header-search-list__item';
        rowLine.innerHTML = `${searchListArr[i]}`;
        searchList.append(rowLine);
      }
      if (searchListArr.length > 5) {
        searchList.lastChild.remove();
        searchListArr.pop();
      }
    }
  });
});


function setToLocal() {
  localStorage.setItem('search-data', JSON.stringify(searchListArr));
}

function setToVisibleList(line) {
  line.innerHTML = `${searchListArr[0]}`;
}

function hideSearchList() {
  if (searchList.classList.contains('search-list-show')) {
    searchList.classList.remove('search-list-show');
  }
}

window.addEventListener('keydown', function(evt) {
  if (evt.keyCode === 27 && searchList) {
    evt.preventDefault();
    hideSearchList();
  }
});

window.addEventListener('click', function(evt) {
  if (evt.target.value === document.innerHTML && searchList) {
    hideSearchList();
  }
});