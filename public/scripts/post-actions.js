const posts = document.querySelectorAll('.section-account__card--custom');
const like = document.querySelectorAll('.like-icon');
const postDelete = document.querySelectorAll('.post-delete');

function sendRequest(url, id) {
  // const requestBody = {id: id};
  const requestBody = id;

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.responseType = 'json';
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.send(JSON.stringify(requestBody));

    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(xhr.response);
      } else {
        resolve(xhr.response);
      }
    };
  });
}

function likeAction(target, data) {
  target.classList.remove('unliked');
  target.classList.add('liked');
  target.src = `${window.location.origin}/public/img/icons/liked_icon.png`;
  data = data || 0;
  target.nextElementSibling.textContent = data;
  // console.log(data);
}

function unlikeAction(target, data) {
  target.classList.remove('liked');
  target.classList.add('unliked');
  target.src = `${window.location.origin}/public/img/icons/unliked_icon.png`;
  data = data || 0;
  target.nextElementSibling.textContent = data;
  // console.log(data);
}

function likeExec(e) {
  let url;
  switch (e.target.classList[1]) {
    case 'unliked':
      url = new URL('post/like', window.location.origin);
      sendRequest(url, e.target.id).then((data) => likeAction(e.target, data));
      break;
    case 'liked':
      url = new URL('post/unlike', window.location.origin);
      sendRequest(url, e.target.id).then((data) => unlikeAction(e.target, data));
      break;
  }
}

like.forEach((el) => el.addEventListener('click', likeExec));

function deletepostAction(data) {
  for (let i = 0; i < posts.length; i++) {
    if (posts[i].id === data) {
      posts[i].remove();
      break;
    }
  }
}

postDelete.forEach((el) => {
  el.addEventListener('click', (e) => {
    const url = new URL('post/delete', window.location.origin);
    sendRequest(url, e.target.id).then((data) => deletepostAction(data));
  });
});
