import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import {
  Account,
  Edit,
  Signup,
  Login,
  Search,
  Error,
  Homepage,
  Logout,
  Messages,
  FriendsPage,
} from './components';

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/user" component={Account} />
          <Route path="/user/homepage" component={Homepage} />
          <Route path="/user/messages" component={Messages} />
          <Route path="/user/edit" component={Edit} />
          <Route path="/user/search" component={Search} />
          <Route path="/user/login" component={Login} />
          <Route path="/user/logout" component={Logout} />
          <Route path="/user/signup" component={Signup} />
          <Route exact path="/user/:username" component={Account} />
          <Route exact path="/user/:username/followers" component={FriendsPage} />
          <Route exact path="/user/:username/following" component={FriendsPage} />
          <Route exact path="*" component={Error} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
