import { CREATE_POST, GET_ALL_POSTS, LIKE_ACTION, READ_USER_POSTS } from './types';

const initialState = {
  posts: [],
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_POST:
      return { ...state, posts: [action.payload, ...state.posts] };
    case READ_USER_POSTS:
      return { ...state, posts: action.payload };
    case GET_ALL_POSTS:
      return { ...state, posts: action.payload };
    case LIKE_ACTION:
      const temp = state.posts.map((post) =>
        post.id === action.payload.id
          ? {
              ...post,
              isLiked: post.isLiked === 'liked' ? 'unliked' : 'liked',
              likes: action.payload.likesCount,
            }
          : { ...post },
      );
      return { ...state, posts: [...temp] };
    default:
      return state;
  }
};
