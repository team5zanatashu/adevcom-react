import {
  CREATE_POST,
  EDIT_USER_ERROR,
  EDIT_USER_INFO,
  EDIT_USER_SUCCESS,
  GET_ALL_POSTS,
  GET_DEFAULT_USER,
  GET_USER_INFO,
  LIKE_ACTION,
  READ_USER_POSTS,
  SEARCH_USERS,
} from './types';

export function createPost(body) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/post/create`, {
      method: 'POST',
      credentials: 'include',
      body,
    });

    const data = await resp.json();

    const lastPost = data.mes.userPosts[0];
    dispatch({ type: CREATE_POST, payload: lastPost });
    // dispatch(readUserPosts(lastPost.username));
  };
}

export function readUserPosts(username) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/account/${username}`, {
      credentials: 'include',
    });
    const data = await resp.json();
    dispatch({ type: READ_USER_POSTS, payload: data.mes.userPosts });
  };
}

export function getAllPosts() {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/homepage`, {
      credentials: 'include',
    });
    const data = await resp.json();

    dispatch({ type: GET_ALL_POSTS, payload: data.mes.userPosts });
  };
}

export function getUserInfo(username) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/account/${username}`, {
      credentials: 'include',
    });
    const data = await resp.json();
    dispatch({ type: GET_USER_INFO, payload: data.mes.user });
  };
}

export function EditUserInfo(body) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/edit`, {
      method: 'POST',
      credentials: 'include',
      body,
    });

    const data = await resp.json();

    // console.log(data);

    if (data.status === 'ok') {
      dispatch({ type: EDIT_USER_SUCCESS, payload: 'Success' });
      dispatch({ type: EDIT_USER_ERROR, payload: '' });
      dispatch({ type: EDIT_USER_INFO, payload: data.mes.user });
      // console.log(data.mes);
      // document.cookie = `username=${data.mes.username}; max-age=3600`;
    } else if (data.status === 'exists') {
      dispatch({ type: EDIT_USER_ERROR, payload: data.mes });
      dispatch({ type: EDIT_USER_SUCCESS, payload: '' });
    }
  };
}

export function likeAction(id) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/post/like`, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(id),
    });
    const data = await resp.json();
    dispatch({ type: LIKE_ACTION, payload: { id: id.toString().slice(1), likesCount: data.mes } });
  };
}

export function searchUsers(body) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/search`, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify({ query: body }),
    });
    const data = await resp.json();
    dispatch({ type: SEARCH_USERS, payload: data.mes.result });
  };
}

export function getDefaultUser(username) {
  return async (dispatch) => {
    const resp = await fetch(`http://devcom/api/user/account/${username}`, {
      credentials: 'include',
    });
    const data = await resp.json();
    dispatch({ type: GET_DEFAULT_USER, payload: data.mes.user });
  };
}
