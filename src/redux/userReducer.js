import {
  EDIT_USER_ERROR,
  EDIT_USER_INFO,
  EDIT_USER_SUCCESS,
  GET_DEFAULT_USER,
  GET_USER_INFO,
  SEARCH_USERS,
} from './types';

const initialState = {
  userInfo: {},
  editError: '',
  editSuccess: '',
  defaultUser: {},
  searchUser: {},
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_INFO:
      return { ...state, userInfo: action.payload };
    case GET_DEFAULT_USER:
      return { ...state, defaultUser: action.payload };
    case EDIT_USER_INFO:
      return { ...state, defaultUser: action.payload };
    case EDIT_USER_ERROR:
      return { ...state, editError: action.payload };
    case EDIT_USER_SUCCESS:
      return { ...state, editSuccess: action.payload };
    case SEARCH_USERS:
      return { ...state, searchUser: action.payload };
    default:
      return state;
  }
};
