export const CREATE_POST = 'CREATE_POST';
export const READ_USER_POSTS = 'READ_USER_POSTS';
export const GET_ALL_POSTS = 'GET_ALL_POSTS';
export const LIKE_ACTION = 'LIKE_ACTION';
export const GET_USER_INFO = 'GET_USER_INFO';
export const EDIT_USER_INFO = 'EDIT_USER_INFO';
export const GET_DEFAULT_USER = 'GET_DEFAULT_USER';
export const SEARCH_USERS = 'SEARCH_USERS';
export const EDIT_USER_ERROR = 'EDIT_USER_ERROR';
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';

// export const SHOW_LOADER = 'SHOW_LOADER';
// export const HIDE_LOADER = 'HIDE_LOADER';
