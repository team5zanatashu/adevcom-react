import React, { useState } from 'react';

export function Default() {
  return (
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
        <title>TITLE default</title>
        <link
          rel="shortcut icon"
          href="../../public/img/icons/a-teamwitter-logo.png"
          type="image/x-icon"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap"
          rel="stylesheet"
        />
        {/*    <link */}
        {/*        rel="stylesheet" */}
        {/*        href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" */}
        {/*        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" */}
        {/*        crossorigin="anonymous" */}
        {/*      /> */}
        {/* <link rel="stylesheet" href="{{ localhost }}public/css/normalize.css" /> */}
        {/*    <link rel="stylesheet" href="{{ localhost }}public/css/outlines.css" /> */}
        {/* <link rel="stylesheet" href="{{ localhost }}public/css/styles1.css" /> */}
        {/* <link rel="stylesheet" href="{{ localhost }}public/css/light-theme.css" /> */}
      </head>
      <body className="body page-body">
        <header className="main-header page-header">
          <h1 className="main-header__page-name">H1</h1>
          <div className="main-header__search">
            <input className="main-header__input" type="text" placeholder="Search here" />
            <div className="main-header__search-icon">
              <img src="{{ localhost }}public/img/icons/icon-search.svg" alt="" />
            </div>
          </div>
        </header>
        {/* <script */}
        {/*      src="https://code.jquery.com/jquery-3.4.1.slim.min.js" */}
        {/*      integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" */}
        {/*      crossorigin="anonymous" */}
        {/*    ></script> */}
        {/*    <script */}
        {/*      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" */}
        {/*      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" */}
        {/*      crossorigin="anonymous" */}
        {/*    ></script> */}
        {/*    <script */}
        {/*      src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" */}
        {/*      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" */}
        {/*      crossorigin="anonymous" */}
        {/*    ></script> */}
        {/* {% block script %}{% endblock %} */}
        {/* <script src="../../public/scripts/err404.js"></script> */}
      </body>
    </html>
  );
}
