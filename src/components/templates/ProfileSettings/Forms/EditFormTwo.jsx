import React, { useState } from 'react';
import './EditFormTwo.scss';
import { useDispatch } from 'react-redux';
import { EditUserInfo } from '../../../../redux/actions';

export function EditFormTwo() {
  const dispatch = useDispatch();

  function SubmitHandler(event) {
    event.preventDefault();

    const body = new FormData(event.target);
    dispatch(EditUserInfo(body));
  }

  return (
    <form onSubmit={(event) => SubmitHandler(event)} className="content-inputs__form" method="POST">
      <h4 className="password-title">Change Password</h4>
      <div className="form-content">
        <div className="form-content__wrapper">
          <div className="form-content__wrapper--first form-content__change-password">
            <input
              type="password"
              className="edit-form__input"
              placeholder="Confirm current password"
              name="password"
            />
            <input
              type="password"
              className="edit-form__input "
              placeholder="Your new password"
              name="password"
            />
            <input
              type="password"
              className="edit-form__input"
              placeholder="Confirm new password"
              name="password"
            />
            {/* <a href="#">Forgot my password</a> */}
          </div>
        </div>
        <button type="submit" className="edit-form__save-button btn btn-primary">
          Change Password
        </button>
      </div>
    </form>
  );
}
