import React, { useState } from 'react';
import './EditFormOne.scss';
import { SignupSelect } from 'Components/templates/Login/SignupSelect/SignupSelect';
import { useDispatch } from 'react-redux';
import { EditUserInfo } from '../../../../redux/actions';

export function EditFormOne() {
  const dispatch = useDispatch();

  function SubmitHandler(event) {
    event.preventDefault();

    const body = new FormData(event.target);
    dispatch(EditUserInfo(body));
  }

  return (
    <form
      onSubmit={(event) => SubmitHandler(event)}
      className="content-inputs__form "
      method="POST"
    >
      <h4 className="registration-title">Personal Information</h4>
      <div className="form-content">
        <div className="form-content__wrapper">
          <div className="form-content__wrapper--first">
            <input
              type="text"
              className="edit-form__input firstName"
              placeholder="First name"
              name="firstname"
            />
            <input
              type="text"
              className="edit-form__input form_input-lastname lastName"
              placeholder="Last name"
              name="lastname"
            />
            <input
              type="email"
              className="edit-form__input email"
              placeholder="Email"
              name="email"
            />
            {/* <input */}
            {/*  type="phone" */}
            {/*  className="edit-form__input email" */}
            {/*  placeholder="Your phone number" */}
            {/*  name="phone" */}
            {/* /> */}
            <input
              type="text"
              className="col-12 mt-3 edit-form__input userName"
              placeholder="Username"
              name="username"
            />
            {/* <input */}
            {/*  type="text" */}
            {/*  className="col-12 mt-3 edit-form__input userName" */}
            {/*  placeholder="Gender" */}
            {/*  name="gender" */}
            {/* /> */}
          </div>
          {/* <p className="hints"> */}
          {/*  Username must be at least three(3) chapters long and must contain one underscore and */}
          {/*  number */}
          {/* </p> */}
          <div className="edit-bio col-12 mt-5  pb-0 pt-0 d-flex flex-wrap">
            <textarea
              className=" edit-form__input edit-form__input--bio w-100 custom-textarea bio pb-2"
              name="bio"
              rows="5"
              id="bio"
              placeholder="Write a little description about you..."
            />
            {/* <label htmlFor="bio" className="edit-label-bio"> */}
            {/*  Bio */}
            {/* </label> */}
          </div>
          <div className="edit-avatar">
            <label htmlFor="profileimg" className="edit-avatar__choose-avatar mb-2 w-100">
              Choose avatar
            </label>
            <input
              type="file"
              name="profileimg"
              accept=".png, .jpg, .jpeg"
              id="profileimg"
              className="inputfile"
            />
          </div>



          <div className="edit-background">
            <label htmlFor="background" className="edit-background__choose-background mb-2 w-100">
              Choose background
            </label>
            <input
              type="file"
              name="background"
              accept=".png, .jpg, .jpeg"
              id="background"
              className="inputfile"
            />
          </div>
          {/* <h6 */}
          {/*  className="card-add-post__card-subtitle-edit card-add-post__card-subtitle" */}
          {/*  id="snum-bio" */}
          {/* > */}
          {/*  0/1000 */}
          {/* </h6> */}
          {/* <div className="mt-3 d-flex date-of-birth justify-content-between flex-row align-items-center"> */}
          {/* <span className="dob-text">Date of birth:</span> */}
          <div className="date-of-birth">
            <span className="dob-text">Date of birth:</span>
            <div className="select-dob-wrapper">
              <SignupSelect option="month" />
              <SignupSelect option="day" />
              <SignupSelect option="year" />
            </div>
          </div>
          {/* <div className="select-dob-wrapper"> */}
          {/*  <span></span> */}
          {/*  <SignupSelect option="month" /> */}
          {/*  <SignupSelect option="day" /> */}
          {/*  <SignupSelect option="year" /> */}
          {/* <select */}
          {/*  className="form-control select-field dob-month edit-form__input edit-form__select" */}
          {/*  name="dob_month" */}
          {/* > */}
          {/*  <option>month</option> */}
          {/*  <option id="0{{ i }}" value="0{{ i }}" /> */}
          {/*  <option id="{{ i }}" value="{{ i }}" /> */}
          {/* </select> */}
          {/* <select */}
          {/*  className="form-control select-field dob-day edit-form__input edit-form__select" */}
          {/*  name="dob_day" */}
          {/* > */}
          {/*  <option value="day">day</option> */}
          {/*  <option id="0{{ i }}" value="0{{ i }}" /> */}
          {/*  <option id="{{ i }}" value="{{ i }}" /> */}
          {/* </select> */}
          {/* <select */}
          {/*  className="form-control select-field dob-year edit-form__input edit-form__select" */}
          {/*  name="dob_year" */}
          {/* > */}
          {/*  <option>year</option> */}
          {/*  <option id="{{ i }}" value="{{ i }}" /> */}
          {/* </select> */}
          {/* </div> */}
          <div>
            {/* /> */}
            {/* <input */}
            {/*  type="text" */}
            {/*  className="col-12 mt-3 edit-form__input edit-form__input--social userName" */}
            {/*  placeholder="Facebook" */}
            {/*  name="username" */}
            {/* /> */}
            {/* <input */}
            {/*  type="text" */}
            {/*  className="col-12 mt-3 edit-form__input edit-form__input--social userName" */}
            {/*  placeholder="Twitter" */}
            {/*  name="username" */}
            {/* /> */}
            {/* <input */}
            {/*  type="text" */}
            {/*  className="col-12 mt-3 edit-form__input edit-form__input--social userName" */}
            {/*  placeholder="GitHub" */}
            {/*  name="username" */}
            {/* /> */}
            {/* <input */}
            {/*  type="text" */}
            {/*  className="col-12 mt-3 edit-form__input edit-form__input--social userName" */}
            {/*  placeholder="Instagram" */}
            {/*  name="username" */}
            {/* /> */}
          </div>
        </div>
        {/* <div className="row withoutPadding"> */}

        {/* </div> */}
        <button
          type="submit"
          className="edit-form__save-button btn btn-primary rounded-pill registrationBtn updateBtn"
        >
          Save Changes
        </button>
        {/* </div> */}
      </div>
    </form>
  );
}
