import React from 'react';
import PropTypes from 'prop-types';
import './ProfileSettings.scss';

function CustomLink(props) {
  const { title, action } = props;

  return (
    <a href="#" className="settings__submenu-link" onClick={(e) => action(e, title)}>
      {title}
    </a>
  );
}

export function ProfileSettings(props) {
  const { title, linkArray, accordionAction } = props;

  return (
    <li key={title} className="settings__item accordion">
      <a
        href="#"
        className="settings__item-link"
        onClick={(e) => {
          accordionAction(e);
        }}
      >
        {title}
      </a>
      <div id="" className="settings__submenu">
        {linkArray.map((element) => (
          <CustomLink key={element.title} title={element.title} action={element.action} />
        ))}
      </div>
    </li>
  );
}

ProfileSettings.defaultProps = {
  title: 'default',
  linkArray: [],
  accordionAction: () => '',
};

ProfileSettings.propTypes = {
  title: PropTypes.string,
  linkArray: PropTypes.array,
  accordionAction: PropTypes.func,
};

CustomLink.defaultProps = {
  title: 'default',
  action: () => '',
};

CustomLink.propTypes = {
  title: PropTypes.string,
  action: PropTypes.func,
};
