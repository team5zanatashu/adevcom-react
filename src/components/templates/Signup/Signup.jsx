import React, { useState } from 'react';
import { SignupModal } from '../index';

// import { Header } from './templates/Header.jsx';
// import MenuScript from '../../public/scripts/menu-script';
// import RegistrationScript from '../../public/scripts/registration-script'



export function Signup() {
  return (
    <div>
      <main className="main">
        {/* <Header /> */}
        <div className="container">
          <form
            action=""
            method="POST"
            className="registration-form col-xl-8 col-lg-10 col-md-12 col-sm-12 col-12 shadow mt-4 form-reg"
          >
            <div className="form-content">
              <h4 className="registration-title">Create an account</h4>
              <SignupModal />
              <div className="row withoutPadding">
                <input
                  type="text"
                  name="firstname"
                  value="firstname"
                  className=" col-xl-5 col-lg-5 col-sm-12 col-12 mt-3 registration-form_input firstName firstname "
                  placeholder="First name"
                />
                <p className="reg-errors" />
                <input
                  type="text"
                  name="lastname"
                  value="lastname"
                  className="col-xl-5 col-lg-5 col-sm-12 col-12 mt-3 ml-auto registration-form_input form_input-lastname lastName lastname"
                  placeholder="Last name"
                />
                <p className="reg-errors" />
              </div>
              <div className="row withoutPadding">
                <input
                  type="email"
                  name="email"
                  value="email"
                  className="col-12 mt-3 registration-form_input email"
                  placeholder="Email"
                />
                <p className="reg-errors" />
                <input
                  type="text"
                  name="username"
                  value="username"
                  className="col-12 mt-3 registration-form_input userName username"
                  placeholder="Username"
                />
                <p className="reg-errors" />
                <p className="hints">
                  Username must be at least three(3) chapters long and must contain one underscore
                  and number
                </p>
                <input
                  type="date"
                  name="dob"
                  className="col-12 mt-2 registration-form_input dateOfBirth "
                  placeholder="Date of birth"
                />
                <div className="date-of-birth">
                  <span className="dob-text">Date of birth:</span>
                  <div className="select-dob-wrapper d-flex justify-content-between">
                    <select className="form-control select-field dob-month" name="dob_month">
                      <option>month</option>
                      <option id="0{{ i }}" value="0{{ i }}">
                        1
                      </option>
                      <option id="{{ i }}" value="{{ i }}">
                        2
                      </option>
                    </select>
                    <select className="form-control select-field dob-day" name="dob_day">
                      <option value="day">day</option>
                      <option id="0{{ i }}" value="0{{ i }}">
                        3
                      </option>
                      <option id="{{ i }}" value="{{ i }}">
                        4
                      </option>
                    </select>
                    <select className="form-control select-field dob-year" name="dob_year">
                      <option>year</option>
                      <option id="{{ i }}" value="{{ i }}">
                        5
                      </option>
                    </select>
                  </div>
                </div>

                <input
                  type="password"
                  name="password"
                  value="{{ formdata.password }}"
                  className="col-12 mt-3 registration-form_input password"
                  placeholder="Password"
                />
                <p className="reg-errors" />
                <p className="hints">
                  Password must be at least eight(8) chapters long and must contain one uppercase
                  and number
                </p>
                <input
                  type="password"
                  name="password"
                  className="col-12 mt-3 registration-form_input confirmPassword confirmpassword"
                  placeholder="Confirm password"
                />
                <button
                  type="submit"
                  name="submit"
                  className="btn btn-primary rounded-pill mt-4 form-button registrationBtn submit "
                >
                  SUBMIT
                </button>
              </div>
            </div>
            <footer className="form-footer col-12">
              <p className="footer-link">
                Already using twitter? <a href="#">LOGIN</a>
              </p>
            </footer>
          </form>
        </div>
      </main>
    </div>
  );
}
