import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import mainLogoLarge from 'Assets/icons/dev_communication-white.svg';

import './Header.scss';

import styled from 'styled-components';

import { useDispatch } from 'react-redux';
import SearchIcon from '../../../assets/icons/icon-search.svg';
import { searchUsers } from '../../../redux/actions';

const welcomeColors = {
  wrapper: 'none',
  search: 'none',
  font: 'white',
  placeholder: '#bdbbc3',
  logoFilter: 'grayscale(100%) saturate(1) invert(100%);',
  mainLogo: 'inline-block',
};

const defaultColors = {
  wrapper: '#FBF9FA',
  search: 'white',
  font: '#111112',
  placeholder: '#7f7d84',
  logoFilter: 'none',
  mainLogo: 'none',
};

function setStyled(pageName) {
  const headerColors = pageName === 'welcome' ? welcomeColors : defaultColors;
  const HeaderWrapper = styled.header`
    background: ${headerColors.wrapper};
    color: ${headerColors.font};
  `;
  const HeaderSearch = styled.input`
    background: ${headerColors.search};
    color: ${headerColors.font};
    ::placeholder {
      color: ${headerColors.placeholder};
    }
  `;
  const SearchLogo = styled.img`
    filter: ${headerColors.logoFilter};
  `;
  const MainLogo = styled.img`
    display: ${headerColors.mainLogo};
  `;
  return { HeaderWrapper, HeaderSearch, SearchLogo, MainLogo };
}

export function Header(props) {
  let queryBody = '';
  const dispatch = useDispatch();

  function searchHandler(event) {
    dispatch(searchUsers(queryBody));
  }

  const [HeaderWrapper, setHeaderWrapper] = useState('header');
  const [HeaderSearch, setHeaderSearch] = useState('input');
  const [SearchLogo, setSearchLogo] = useState('img');
  const [MainLogo, setMainLogo] = useState('img');
  const { pageName } = props;
  useEffect(() => {
    const { HeaderWrapper, HeaderSearch, SearchLogo, MainLogo } = setStyled(pageName);
    setHeaderWrapper(HeaderWrapper);
    setHeaderSearch(HeaderSearch);
    setSearchLogo(SearchLogo);
    setMainLogo(MainLogo);
  }, [pageName]);
  return (
    <HeaderWrapper className="main-header page-header">
      <h1 className="main-header__page-name">{pageName}</h1>
      <MainLogo className="main-header__logo-img-large" src={mainLogoLarge} alt="Main Logo" />

      <div className="main-header__search">
        <HeaderSearch
          autoFocus={pageName === 'welcome'}
          className="main-header__input"
          type="text"
          placeholder="Search here..."
          onChange={(event) => {
            queryBody = event.target.value;
          }}
        />
        <div
          className="main-header__search-icon"
          onClick={(event) => {
            if (queryBody) {
              searchHandler(event);
            }
          }}
        >
          <Link to="/user/search">
            <SearchLogo src={SearchIcon} alt={pageName} />
          </Link>
        </div>
      </div>
    </HeaderWrapper>
  );
}

Header.propTypes = {
  pageName: PropTypes.string,
};
