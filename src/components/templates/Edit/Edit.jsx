import React, { useState } from 'react';

import {
  Header,
  EditFormOne,
  EditFormTwo,
  LeftSidebar,
  RightSidebar,
  ProfileSettings,
} from '../index';

import './Edit.scss';
import { useSelector } from 'react-redux';
// import profileBack from '../../../assets/img/sea1.jpg';

export function Edit() {
  const [form, setForm] = useState('Personal Information');

  const formSwitcher = (e, formName = form) => {
    setForm(formName);

    if (e.target.classList.contains('active')) {
      e.target.classList.remove('active');
    } else {
      e.target.classList.add('active');
    }
  };

  const editError = useSelector((state) => state.user.editError);
  const editSuccess = useSelector((state) => state.user.editSuccess);

  document.title = 'Edit';
  return (
    // <div className="Edit">
    //   <div className="header-section sticky-top">
    //     <Header pageName="Edit Profile" />
    //   </div>
    //   <main className="edit-main">
    //     <div className="edit-main__wrapper">
    //       <LeftSidebar />
    //       <RightSidebar />
    //       <div className="section-edit">
    //         {/*<HeaderBackground profileBack={profileBack} size="full" />*/}
    //         <div className="section-edit__background-image">
    //           <span className="section-edit__dashboard-text">Your Account Dashboard</span>
    //         </div>
    //         <div className="section-edit__content">
    //           <div className="section-edit__content-preferences">

    //             <ul className="profile-settings">
    //               <span>Your Profile</span>
    //               <ProfileSettings
    //                 title="Profile Settings"
    //                 accordionAction={formSwitcher}
    //                 linkArray={[
    //                   { title: 'Personal Information', action: formSwitcher },
    //                   { title: 'Change Password', action: formSwitcher },
    //                 ]}
    //               />
    //               <ProfileSettings
    //                 title="Interface Settings"
    //                 accordionAction={formSwitcher}
    //                 linkArray={[
    //                   { title: 'Fonts', action: formSwitcher },
    //                   { title: 'Colors', action: formSwitcher },
    //                   { title: 'Messanger', action: formSwitcher },
    //                 ]}
    //               />
    //             </ul>

    //           </div>

    //           <div className="section-edit__content-inputs">
    //             {form === 'Personal Information' && <EditFormOne />}
    //             {form === 'Change Password' && <EditFormTwo />}
    //           </div>

    //         </div>
    //       </div>
    //     </div>
    //   </main>
    // </div>

    <div>
      <Header pageName="Edit" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <RightSidebar />
          {/* <div className="section-edit__background-image"> */}
          {/*  <span className="section-edit__dashboard-text">Your Account Dashboard</span> */}
          {/* </div> */}
          <div>
            <section className="section-edit__main">
              <div className="section-edit__column-one">
                <div className="section-edit__profile-settings">
                  {/* <ProfileIntro /> */}
                  <ul className="profile-settings">
                    <span className="profile-settings__title">Your Profile</span>
                    <ProfileSettings
                      title="Profile Settings"
                      accordionAction={formSwitcher}
                      linkArray={[
                        { title: 'Personal Information', action: formSwitcher },
                        { title: 'Change Password', action: formSwitcher },
                      ]}
                    />
                    {/* <ProfileSettings */}
                    {/*  title="Interface Settings" */}
                    {/*  accordionAction={formSwitcher} */}
                    {/*  linkArray={[ */}
                    {/*    { title: 'Fonts', action: formSwitcher }, */}
                    {/*    { title: 'Colors', action: formSwitcher }, */}
                    {/*    { title: 'Messanger', action: formSwitcher }, */}
                    {/*  ]} */}
                    {/* /> */}
                  </ul>
                </div>
              </div>
              <div className="section-edit__column-two">
                <section className="section-edit__main-column">
                  <div className="section-edit__content-inputs content-inputs">
                    {editError !== '' && (
                      <h4 className="registration-title edit-error">{editError}</h4>
                    )}
                    {editSuccess !== '' && (
                      <h4 className="registration-title edit-success">{editSuccess}</h4>
                    )}

                    {form === 'Personal Information' && <EditFormOne />}
                    {form === 'Change Password' && <EditFormTwo />}
                  </div>
                </section>
              </div>
            </section>
          </div>
        </div>
      </main>
    </div>
  );
}
