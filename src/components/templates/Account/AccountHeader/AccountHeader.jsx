import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import './AccountHeader.scss';
import friendsImg from 'Assets/icons/friends.svg';
import friendsActiveImg from 'Assets/icons/friends-active.svg';
import messageImg from 'Assets/icons/messenger.svg';
import { subscribeRequest } from 'Components/templates/FriendsPage/FriendsList/subscribeRequest';

// import friendsActiveImg from 'Assets/icons/friends-active.svg';

export function AccountHeader() {
  const userInfo = useSelector((state) => state.user.userInfo);
  // const [posts, setPosts] = useState([]);
  // const [error, setError] = useState(null);
  const [subscribeImg, setSubscribe] = useState(friendsImg);
  useEffect(() => {
    const img = userInfo.isMeSubscribed ? friendsActiveImg : friendsImg;
    setSubscribe(img);
  }, [userInfo]);

  function subscribeHandler(e) {
    e.preventDefault();
    const subscribeSubject = userInfo.username;
    subscribeRequest('devcom', 'change', subscribeSubject).then(({ status, mes }) => {
      if (status === 'ok') {
        // localStorage.setItem('user', mes);
        const img = mes === 'subscribed' ? friendsActiveImg : friendsImg;
        setSubscribe(img);
        console.log(mes);
      } else {
        console.log(mes);
      }
    });
  }
  function tempLogout() {
    // localStorage.removeItem('user');
  }
  return (
    <section className="section-account__header">
      <div
        className="section-account__header-img"
        style={{ backgroundImage: `url(${userInfo.background})` }}
      />
      {!userInfo.isMe && (
        <div className="section-account__header-interactions">
          <button className="interaction-button" onClick={subscribeHandler}>
            <img src={subscribeImg} alt="friends" width="40" height="40" />
          </button>
          <button className="interaction-button" onClick={tempLogout}>
            <img src={messageImg} alt="friends" width="40" height="40" />
          </button>
        </div>
      )}
      <nav className="section-account__nav-menu account-nav-menu">
        <ul className="section-account__menu-list">
          {/* <li className="section-account__menu-item"> */}
          {/*  /!* <a className="section-account__menu-link" href="#"> *!/ */}
          {/*  /!*  Homepage *!/ */}
          {/*  /!* </a> *!/ */}
          {/*  <Link to="/user/homepage" className="section-account__menu-link"> */}
          {/*    Homepage */}
          {/*  </Link> */}
          {/* </li> */}
          <li className="section-account__menu-item">
            <a className="section-account__menu-link" href="#">
              About
            </a>
          </li>

          <li className="section-account__menu-item">
            <Link
              to={`/user/${userInfo.username}/followers`}
              className="section-account__menu-link"
            >
              Followers
            </Link>
          </li>
          {/* <li className="section-account__menu-item"> */}
          {/*  <a className="section-account__menu-link" href="#"> */}
          {/*    Photos */}
          {/*  </a> */}
          {/* </li> */}
        </ul>
        <div className="account-nav-menu__avatar-block">
          <img src={userInfo.profileimg} alt="user photo" className="account-nav-menu__avatar" />
          <span className="account-nav-menu__avatar-name">
            {userInfo.firstname} {userInfo.lastname}
          </span>
          <span className="account-nav-menu__avatar-username">@{userInfo.username}</span>
        </div>
        <ul className="section-account__menu-list">
          <li className="section-account__menu-item">
            <Link
              to={`/user/${userInfo.username}/following`}
              className="section-account__menu-link"
            >
              Following
            </Link>
          </li>
          <li className="section-account__menu-item">
            {/* <a className="section-account__menu-link" href="#"> */}
            {userInfo.isMe ? (
              <Link to="/user/edit" className="section-account__menu-link">
                Edit
              </Link>
            ) : (
              <a href="#" className="section-account__menu-link">
                Projects
              </a>
            )}

            {/* </a> */}
          </li>
          {/* <li className="section-account__menu-item"> */}
          {/*  <a */}
          {/*    href="https://github.com/" */}
          {/*    target="_blank" */}
          {/*    rel="noreferrer" */}
          {/*    className="section-account__menu-link" */}
          {/*  > */}
          {/*    Projects */}
          {/*  </a> */}
          {/* </li> */}
        </ul>
      </nav>
    </section>
  );
}
