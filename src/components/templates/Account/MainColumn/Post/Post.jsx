import React, { useEffect, useState } from 'react';
import './Post.scss';
import { Link } from 'react-router-dom';
import Like from 'Assets/icons/liked_icon.png';
import { useDispatch } from 'react-redux';
import { likeAction } from '../../../../../redux/actions';

export function Post(props) {
  const dispatch = useDispatch();

  const { post } = props;
  // console.log(post);
  const {
    user_id,
    username,
    firstname,
    lastname,
    email,
    body,
    created_at,
    profileimg,
    image,
    likes,
    isLiked,
  } = post;
  const likedImgPath = `http://devcom/public/img/icons/${isLiked}_icon.png`;
  const id = `p${user_id}`;
  const likeId = `l${post.id}`;
  const delId = `del${post.id}`;
  const accountLink = `/user/${username}`;
  return (
    <li className="section-account__card section-account__card--custom" id={id}>
      <div className="section-account__post-header">
        <div className="section-account__name-info-wrap">
          <img className="section-account__post-profile-img" src={profileimg} alt="Profile image" />
          {/* <span className="card-subtitle-name"> */}
          {/* { firstname } {lastname } @{ username } */}
          <Link className="card-subtitle-name" to={accountLink}>
            @{username}
          </Link>
          {/* </span> */}
          <h6 className="card-subtitle-date">{created_at}</h6>
        </div>
        <div className="section-account__add-functions">
          <span className="section-account__span" />
          <span className="section-account__span" />
          <span className="section-account__span" />
        </div>
        <div className="post-footer__link display-none">
          <a href="#" className="card-link d-none">
            View more photos
          </a>
          {/* {% if post.canDel == true %} */}
          <span className="post-delete" id={delId}>
            Delete
          </span>
          {/* {% endif %} */}
        </div>
      </div>
      <p className="card-text text-body font-size-16">{body}</p>
      {image && <img src={image} className="card-img-top rounded-0" />}
      <div className="section-account__post-footer">
        <div className="section-account__post-footer-actions">
          <div className="actions__like">
            <img
              onClick={(event) => {
                dispatch(likeAction(event.target.id));
              }}
              src={likedImgPath}
              className="like-icon"
              id={likeId}
              alt={Like}
            />
            <span className="like-number font-weight-bold font-size-12">{likes}</span>
          </div>

          {/*    <div className="mr-2 actions__rt">  */}
          {/* <img src="../../public/img/icons/rt_icon.png" className="rt-icon" />  */}
          {/*       <span className="rt-number font-weight-bold font-size-12">35</span>  */}
          {/*     </div>  */}
          {/*            <div className="mr-2 actions__fav">  */}
          {/*     <img src="../../public/img/icons/fav_icon.png" className="fav-icon" />  */}
          {/*       <span className="fav-number font-weight-bold font-size-12">99</span>  */}
          {/*      </div>  */}
          {/*            <div className="mr-2 actions__more">  */}
          {/*  <img src="../../public/img/icons/more_icon.png" className="more-icon" />  */}
          {/*            </div>  */}
        </div>
      </div>
    </li>
  );
}
