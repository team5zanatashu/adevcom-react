import React from 'react';
import './MainColumn.scss';

import Posts from './Posts/Posts';
import { PostsFrom } from './PostsForm/PostsForm';

export function MainColumn(props) {
  const { isMe = true, homePage = false } = props;

  return (
    <div className=" section-account__card-add-post card-add-post">
      {(isMe || homePage) && <PostsFrom />}
      <Posts />
    </div>
  );
}
