import React, { useState } from 'react';
import './PostsFrom.scss';
import { useDispatch } from 'react-redux';
import iconFile from 'Assets/icons/file-upload.svg';
import { createPost } from '../../../../../redux/actions';

export function PostsFrom() {
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const [textLength, setLength] = useState(0);
  const [file, setFile] = useState('');

  function SubmitHandler(event) {
    event.preventDefault();

    const body = new FormData(event.target);
    dispatch(createPost(body));
    setText('');
  }

  return (
    <form
      method="POST"
      className="card-add-post__body card-add-post__form"
      onSubmit={(event) => {
        SubmitHandler(event);
      }}
    >
      <div className="card-add-post__text-wrapper">
        {/* <div className="card-add-post__photo-wrapper" /> */}
        <textarea
          rows="1"
          name="post_field"
          className=" card-add-post__input"
          placeholder="Share what you are thinking here..."
          id="input-post"
          value={text}
          onChange={(e) => {
            setText(e.target.value);
            setLength(e.target.value.toString().length);
          }}
        />
      </div>
      <div className="card-add-post_functions-wrapper">
        <div className="card-add-post__functions-inner">
          <img className="card-add-post__post-image" src={iconFile} alt="icon file" />
          <input
            type="file"
            name="image"
            className="card-add-post__post-input"
            accept="image/*"
            onChange={(event) => {
              setFile(event.target.files[0].name);
            }}
          />
          {file && <span className="card-add-post__file-name">{file}</span>}
          <h6 className="card-add-post__card-subtitle" id="snum-post">
            {textLength}/1000
          </h6>
        </div>
        <input type="submit" className="card-add-post__button" id="add-post" value="POST" />
      </div>
    </form>
  );
}
