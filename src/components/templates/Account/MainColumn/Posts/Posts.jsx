import { Post } from 'Components/templates';
import React from 'react';
import './Posts.scss';
import { useSelector } from 'react-redux';

export default function Posts(props) {
  const posts = useSelector((state) => state.posts);
  return (
    <>
      <ul className="section-account__posts-list">
        {posts && posts.posts.map((post) => <Post key={post.id} post={post} />)}
      </ul>
    </>
  );
}
