import React, { useState } from 'react';

import MessageIcon from 'Assets/icons/messages.svg';
import ProfilePhoto from 'Assets/icons/user.svg';

import './RightSidebar.scss';

export function RightSidebar() {
  const [isChecked, setChecked] = useState(true);

  return (
    <div>
      <input
        type="checkbox"
        name="menu-checkbox"
        className="right-sidebar__nav-check "
        checked={isChecked}
        onChange={() => {
          setChecked(!isChecked);
        }}
      />
      <div className="right-sidebar__open-close-button">
        <span>&#123;</span>
      </div>

      <div className="right-sidebar">
        <div className="right-sidebar__messages-list">
          {/* <div className="right-sidebar__messages-block"> */}
          <ul className="right-sidebar__messages-list--inner">
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
            <li className="right-sidebar__messages-list-item">
              <img className="right-sidebar__user-photo" src={ProfilePhoto} alt="profile photo" />
              <span className="right-sidebar__user-name">Name Surname</span>
            </li>
          </ul>
          <div
            className="right-sidebar__messages-list-icon"
            onClick={() => {
              setChecked(!isChecked);
            }}
          >
            <img className="right-sidebar__messenger-icon" src={MessageIcon} alt="messenger" />
          </div>
          {/* </div> */}
        </div>

        {/* <div className="right-sidebar__messenger-icon-wrapper"> */}
        {/*  <div className="right-sidebar__messenger-icon"> */}
        {/*    <svg id="olymp-comments-post-icon" viewBox="0 0 36 32" width="100%" height="100%"> */}
        {/*      <title>comments-post-icon</title> */}
        {/*      <path d="M32 0h-28c-2.21 0-4 1.792-4 4v18c0 2.208 1.792 4 4 4 0 0 0.75 0 2 0v-4h-2v-18h28v22c2.208 0 4-1.792 4-4v-18c0-2.208-1.792-4-4-4zM18 26h2v-4h-2v4zM24 26h4v-4h-4v4zM8 12h20v-4h-20v4zM8 18h12v-4h-12v4z" /> */}
        {/*      <path d="M18 22l-8 4.282v-4.282h-4v10l12-6v-4z" /> */}
        {/*    </svg> */}
        {/*  </div> */}
        {/* </div> */}
      </div>
    </div>
  );
}
