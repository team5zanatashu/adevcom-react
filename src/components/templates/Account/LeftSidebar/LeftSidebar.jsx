import React, { useEffect, useState } from 'react';
import './LeftSidebar.scss';
import { Link } from 'react-router-dom';
import iconClose from 'Assets/icons/icon-close.svg';
import iconHomepage from 'Assets/icons/icons-home.svg';
import mainLogoSmall from 'Assets/icons/dev_main_logo.svg';
import mainLogoLarge from 'Assets/icons/dev_communication.svg';
import iconLogout from 'Assets/icons/logout.svg';
import iconMessage from 'Assets/icons/mes.svg';
import iconUser from 'Assets/icons/user.svg';
import iconEdit from 'Assets/icons/edit.svg';
import iconFollow from 'Assets/icons/follow.svg';
import iconSearch from 'Assets/icons/icon-search-black.svg';

import styled from 'styled-components';
import { useSelector } from 'react-redux';

const logoDimensions = {
  width: '50px',
  height: '50px',
  transform: 'translate(30%, 20%)',
};

function setStyles(isChecked) {
  if (isChecked) {
    logoDimensions.width = '257px';
    logoDimensions.height = '50px';
    logoDimensions.transform = 'translate(3%, 18%)';
  } else {
    logoDimensions.width = '50px';
    logoDimensions.height = '50px';
    logoDimensions.transform = 'translate(30%, 20%)';
  }

  return styled.img`
    width: ${logoDimensions.width};
    height: ${logoDimensions.height};
    transform: ${logoDimensions.transform};
  `;
}

function logoutHandler(e) {
  e.preventDefault();
  fetch('http://devcom/api/user/logout', { credentials: 'include' })
    .then((res) => res.json())
    .then(({ status, mes }) => {
      if (status === 'ok') {
        document.cookie = `username=${mes}; max-age=-1`;
        window.location.href = window.location.origin;
      } else {
        console.log(mes);
      }
    });
}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length == 2) return parts.pop().split(';').shift();
}

export function LeftSidebar() {
  const defaultUser = `/user/${getCookie('username')}`;

  const [isChecked, setChecked] = useState(true);

  const logos = { mainLogoSmall, mainLogoLarge };

  const [selected, setSelected] = useState(logos.mainLogoSmall);

  const [LargeLogo, setLogo] = useState(styled.img``);

  useEffect(() => setLogo(setStyles(!isChecked)), []);

  function changeLogo() {
    setChecked(!isChecked);
    if (isChecked) {
      setSelected(logos.mainLogoLarge);
    } else {
      setSelected(logos.mainLogoSmall);
    }
    setLogo(setStyles(isChecked));
  }
  return (
    <div>
      <input
        type="checkbox"
        name="menu-checkbox"
        className="left-sidebar__nav-check"
        checked={isChecked}
        onChange={changeLogo}
      />
      <div className="left-sidebar__open-close-button">
        <span>&#125;</span>
      </div>
      <div className="left-sidebar">
        <div className="left-sidebar__logo">
          <LargeLogo src={selected} alt="Main logo" className="left-sidebar__logo-img" />
        </div>
        <nav className="left-sidebar__nav-menu">
          <ul className="left-sidebar__nav-list">
            <li className="left-sidebar__nav-item nav-item__collapse-menu">
              <a
                onClick={changeLogo}
                className="left-sidebar__menu-link left-sidebar__menu-link--collapse-menu"
                href="#"
              >
                <img className="left-sidebar__menu-link-icon" src={iconClose} alt="Close icon" />
                Collapse Menu
              </a>
            </li>
            <li className="left-sidebar__nav-item nav-item__account">
              {/* <a className="left-sidebar__menu-link" href="#"> */}
              <Link to={defaultUser} className="left-sidebar__menu-link">
                <img className="left-sidebar__menu-link-icon" src={iconUser} alt="Account icon" />
                Account
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__homepage">
              <Link
                to="/user/homepage"
                className="left-sidebar__menu-link left-sidebar__menu-link--homepage"
              >
                <img
                  className="left-sidebar__menu-link-icon"
                  src={iconHomepage}
                  alt="Homepage icon"
                />
                Homepage
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__message">
              <Link to="/user/messages" className="left-sidebar__menu-link">
                <img
                  className="left-sidebar__menu-link-icon"
                  src={iconMessage}
                  alt="Message icon"
                />
                Messages
              </Link>
            </li>

            <li className="left-sidebar__nav-item nav-item__edit">
              <Link to="/user/edit" className="left-sidebar__menu-link">
                <img className="left-sidebar__menu-link-icon" src={iconEdit} alt="Edit icon" />
                Edit
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__search">
              <Link to="/user/search" className="left-sidebar__menu-link">
                <img className="left-sidebar__menu-link-icon" src={iconSearch} alt="Search icon" />
                Search
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__followers">
              <Link
                to={`/user/${getCookie('username')}/followers`}
                className="left-sidebar__menu-link"
              >
                <img
                  className="left-sidebar__menu-link-icon"
                  src={iconFollow}
                  alt="Followers icon"
                />
                Followers
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__following">
              <Link
                to={`/user/${getCookie('username')}/following`}
                className="left-sidebar__menu-link"
                href="#"
              >
                <img
                  className="left-sidebar__menu-link-icon"
                  src={iconFollow}
                  alt="Following icon"
                />
                Following
              </Link>
            </li>
            <li className="left-sidebar__nav-item nav-item__logout">
              <a href="#" className="left-sidebar__menu-link" onClick={logoutHandler}>
                <img className="left-sidebar__menu-link-icon" src={iconLogout} alt="Close icon" />
                Logout
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
