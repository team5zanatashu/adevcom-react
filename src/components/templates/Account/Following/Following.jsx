import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Following.scss';
import { useSelector } from 'react-redux';

export function Following() {
  const userInfo = useSelector((state) => state.user.userInfo);
  return (
    <div className="friends">
      <div className="friends__header-wrapper">
        <span className="friends__header-title">
          <Link to={`/user/${userInfo.username}/following`} className="friends__header-title-link">
            {/* <a className="friends__header-title-link" href="#"> */}
            Following
            {/* </a> */}
          </Link>
        </span>
      </div>
      <div className="friends__list-wrapper">
        <ul className="friends__list">
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
        </ul>
      </div>
    </div>
  );
}
