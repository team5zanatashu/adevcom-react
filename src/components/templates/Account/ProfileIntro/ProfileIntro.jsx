import React, { useState } from 'react';
import { useSelector } from 'react-redux';

export function ProfileIntro(props) {
  const userInfo = useSelector((state) => state.user.userInfo);
  return (
    <div className="profile-intro">
      <div className="profile-intro__header">
        <span className="profile-intro__title">
          {/* <a className="profile-intro__title-link" href="#"> */}
          Profile Info
          {/* </a> */}
        </span>
      </div>
      <div className="profile-intro__about-wrapper">
        <div className="profile-intro__about-text">
          <h6 className="profile-intro__about-head">About Me:</h6>
          <span className="profile-intro__about-desc">{userInfo.bio}</span>
        </div>
        <div className="profile-intro__social-wrapper">
          {/* <h6 className="profile-intro__social-head">Other Social Networks:</h6> */}
          {/* <div className="profile-intro__social-block"> */}
          {/*  <ul className="profile-intro__social-list"> */}
          {/*    <li className="profile-intro__social-item"> */}
          {/*      <a */}
          {/*        className="profile-intro__social-link profile-intro__social-link--facebook" */}
          {/*        href="#" */}
          {/*      > */}
          {/*        1 */}
          {/*      </a> */}
          {/*    </li> */}
          {/*    <li className="profile-intro__social-item"> */}
          {/*      <a */}
          {/*        className="profile-intro__social-link profile-intro__social-link--twitter" */}
          {/*        href="#" */}
          {/*      > */}
          {/*        2 */}
          {/*      </a> */}
          {/*    </li> */}
          {/*    <li className="profile-intro__social-item"> */}
          {/*      <a */}
          {/*        className="profile-intro__social-link profile-intro__social-link--dribble" */}
          {/*        href="#" */}
          {/*      > */}
          {/*        3 */}
          {/*      </a> */}
          {/*    </li> */}
          {/*  </ul> */}
          {/* </div> */}
        </div>
      </div>
    </div>
  );
}
