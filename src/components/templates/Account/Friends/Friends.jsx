import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Friends.scss';
import { useSelector } from 'react-redux';

export function Friends() {
  const userInfo = useSelector((state) => state.user.userInfo);
  return (
    <div className="friends">
      <div className="friends__header-wrapper">
        <span className="friends__header-title">
          <Link to={`/user/${userInfo.username}/followers`} className="friends__header-title-link">
            Followers
          </Link>
        </span>
      </div>
      <div className="friends__list-wrapper">
        <ul className="friends__list">
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
          <li className="friends__list-item" />
        </ul>
      </div>
    </div>
  );
}
