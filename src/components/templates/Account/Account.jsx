import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  MainColumn,
  ProfileIntro,
  Friends,
  LeftSidebar,
  RightSidebar,
  Header,
  AccountHeader,
} from '../index';
import './Account.scss';
import { getUserInfo, readUserPosts } from '../../../redux/actions';
import { Following } from 'Components/templates/Account/Following/Following';

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length == 2) return parts.pop().split(';').shift();
}

export function Account(props) {
  const {
    match: {
      params: { username },
    },
  } = props;

  const defaultUser = getCookie('username');

  // debugger
  const user = username == undefined ? defaultUser : username; // костыль

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUserInfo(user));
  }, []);

  useEffect(() => {
    dispatch(readUserPosts(user));
  }, []);

  const userInfo = useSelector((state) => state.user.userInfo);
  document.title = userInfo.username;
  return (
    // {% extends "src/components/views/Default.jsx" %}
    <div>
      <Header pageName="profile page" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <RightSidebar />
          <div className="account-main__section-account section-account">
            <AccountHeader />
          </div>
          <div>
            <section className="section-account__main">
              <div className="section-account__column-one">
                <section className="section-account__profile-intro">
                  <ProfileIntro />
                </section>
              </div>
              <div className="section-account__column-two">
                <section className="section-account__main-column">
                  <MainColumn isMe={userInfo.isMe} />
                </section>
              </div>
              <div className="section-account__column-three">
                <section className="section-account__friends">
                  <Friends />
                  <Following />
                </section>
              </div>
            </section>
          </div>
        </div>
      </main>
    </div>
  );
}
