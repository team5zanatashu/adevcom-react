import React from 'react';
import PropTypes from 'prop-types';
import './HeaderBackground.scss';

export function HeaderBackground(props) {
  const { profileBack, size } = props;
  const className = `section__header-background-img ${size}`;
  return (
    <div className="section__header-background">
      <div className={className} style={{ backgroundImage: `url(${profileBack})` }} />
    </div>
  );
}
HeaderBackground.defaultProps = {
  profileBack: '',
  size: 'full',
};

HeaderBackground.propTypes = {
  profileBack: PropTypes.string,
  size: PropTypes.oneOf(['full', 'sm', 'md']),
};
