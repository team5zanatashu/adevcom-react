// import React, { useState } from 'react';
// // import Header from './templates/Header';
// // import UserCard from './templates/UserCard';
// import { RightSidebar, MainColumn, LeftSidebar} from '../index'
// // import { Footer } from './templates/Footer';
//
// // import HeaderSearchList from '../../../public/scripts/header-search-list';
// // import TabsClicker from '../../../public/scripts/tabs-clicker';
// // import TweetScripts from '../../public/scripts/tweet-header-toggle-script';
// // import TweetHeaderToggleScript from '../../public/scripts/tweet-header-toggle-script';
// // import AddTweetValidation from '../../public/scripts/add-tweet-validation';
// // import PostActions from '../../public/scripts/post-actions';
//
// export function Homepage() {
//   return (
//     <section className=" main-section main-bg-color">
//       {/* <Header /> */}
//       <p>Header</p>
//       <div className="main-wrapper">
//         <div className="">
//           {/* <UserCard /> */}
//           <div className="tweets-main">
//             <div className="tab-content">
//               <LeftSidebar />
//               <MainColumn />
//               <RightSidebar />
//               <p>Homepage</p>
//             </div>
//             <div />
//             {/* <Footer /> */}
//             Footer
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// }

import React, { useEffect } from 'react';
import {
  MainColumn,
  ProfileIntro,
  Friends,
  LeftSidebar,
  RightSidebar,
  Header,
  AccountHeader,
} from '../index';
import './Homepage.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getAllPosts, getUserInfo, readUserPosts } from '../../../redux/actions';

export function Homepage() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllPosts());
  }, []);
  document.title = 'Home Page';
  return (
    // {% extends "src/components/views/Default.jsx" %}
    <div>
      <Header pageName="Homepage" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <RightSidebar />
          <div>
            <section className="section-account__main">
              <div className="section-account__column-one">
                <div className="section-account__profile-intro">
                  <ProfileIntro />
                </div>
              </div>
              <div className="section-account__column-two">
                <section className="section-account__main-column">
                  <MainColumn homePage />
                </section>
              </div>
              <div className="section-account__column-three">
                <section className="section-account__friends">{/* <Friends /> */}</section>
              </div>
            </section>
          </div>
        </div>
      </main>
    </div>
  );
}
