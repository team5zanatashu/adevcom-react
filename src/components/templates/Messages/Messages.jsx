import React, { createContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Header, LeftSidebar, MessagesSection } from '../index';
import './Messages.scss';

export function Messages(props) {
  document.title = 'Messanger';
  const {
    location: { state },
  } = props;
  return (
    <>
      <Header pageName="Messages" />
      <main className="messages-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <div>
            <MessagesSection username={state} />
          </div>
        </div>
      </main>
    </>
  );
}

Messages.propTypes = {
  state: PropTypes.object,
};
