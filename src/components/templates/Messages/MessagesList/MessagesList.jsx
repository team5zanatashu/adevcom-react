import React, { useEffect, useState, useContext } from 'react';
import './MessagesList.scss';
// import iconUser from 'Assets/icons/user.svg';
import { timeConversion } from 'Components/templates/Messages/timeConversion';
import { MessagesContext } from '../MessagesSection';

// import { Link } from 'react-router-dom';
// import styled from 'styled-components';

export async function getChat(username) {
  const res = await fetch('http://devcom/api/message/singlechat', {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify({ username }),
  });
  const chat = await res.json();
  return chat;
}

export function MessagesList() {
  const { companions, setMessages, username } = useContext(MessagesContext);

  function chooseDialog(e) {
    const selected = document.querySelector('.dialog-selected');
    if (selected) {
      selected.classList.remove('dialog-selected');
    }
    e.currentTarget.classList.add('dialog-selected');
    const { id } = e.currentTarget;
    getChat(id).then(({ status, mes }) => {
      if (status === 'ok') {
        setMessages(mes);
        document.querySelector('.dialog-window__form-input').value = '';
      } else {
        console.log(mes);
      }
    });
  }

  useEffect(() => {
    if (username) {
      getChat(username).then(({ status, mes }) => {
        if (status === 'ok') {
          setMessages(mes);
          document.querySelector('.dialog-window__form-input').value = '';
        } else {
          console.log(mes);
        }
      });
    }
  }, []);

  return (
    <ul className="messages-list">
      {companions.map(
        (item) => (
          // item.body && (
          <li
            key={`r${item.username}`}
            id={item.username}
            className="messages-list__item"
            onClick={(e) => chooseDialog(e)}
          >
            <img className="messages-list__item-image" src={item.profileimg} alt="user" />
            <div className="messages-list__item-preview">
              <div className="messages-list__item-header">
                <span className="messages-list__user-info">
                  {item.firstname} {item.lastname}
                </span>
                <span className="messages-list__message-time">
                  {timeConversion(item.created_at)}
                </span>
              </div>
              <div className="messages-list__message-preview">
                {item.isMine && <span className="messages-list__message-my">Me: </span>}
                {item.body}
              </div>
            </div>
          </li>
        ),
        // ),
      )}
    </ul>
  );
}
