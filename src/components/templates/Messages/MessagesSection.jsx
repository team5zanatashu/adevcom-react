import { DialogWindow, MessagesList } from 'Components/templates';
import React, { createContext, useEffect, useState } from 'react';

export const MessagesContext = createContext(null);

function sortTime(arr) {
  return arr.sort((a, b) => {
    const mesA = Number(a.created_at.replace(/[:\s-]/g, ''));
    const mesB = Number(b.created_at.replace(/[:\s-]/g, ''));
    return -(mesA - mesB);
    // if (a.created_at < b.created_at) {
    //   return -1;
    // }
    // if (a.created_at > b.created_at) {
    //   return 1;
    // }
    // return 0;
  });
}

async function getCompanions() {
  const res = await fetch('http://devcom/api/message/allchats', { credentials: 'include' });
  const companions = await res.json();
  return companions;
}

export function MessagesSection(props) {
  const [companions, setCompanions] = useState([]);
  const [messages, setMessages] = useState([]);
  const [username, setUsername] = useState('');
  const { state } = props;

  function changeCompanions() {
    getCompanions().then(({ status, mes }) => {
      if (status === 'ok') {
        mes = sortTime(mes);
        setCompanions(mes);
      } else {
        console.log(mes);
      }
    });
  }
  useEffect(() => {
    console.log(state)
    if (state) {
      setUsername(state.username);
    }
    const interval = setInterval(() => {
      changeCompanions();
    }, 5000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    changeCompanions();
  }, [messages]);
  return (
    <MessagesContext.Provider
      value={{ companions, setCompanions, messages, setMessages, username, setUsername }}
    >
      <section className="section-messages__main">
        <MessagesList />
        <DialogWindow />
        {/* <div className="section-account__column-one"> */}
        {/*  <div className="section-account__profile-intro">/!* <ProfileIntro /> *!/</div> */}
        {/* </div> */}
        {/* <div className="section-account__column-two"> */}
        {/*  <section className="section-account__main-column">/!* <MainColumn /> *!/</section> */}
        {/* </div> */}
        {/* <div className="section-account__column-three"> */}
        {/*  <section className="section-account__friends">/!* <Friends /> *!/</section> */}
        {/* </div> */}
      </section>
    </MessagesContext.Provider>
  );
}
