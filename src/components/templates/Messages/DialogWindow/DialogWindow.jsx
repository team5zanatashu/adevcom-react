import React, { useEffect, useState, useContext, useRef } from 'react';
// import { FriendsItem } from '../../index';
import './DialogWindow.scss';
// import iconUser from 'Assets/icons/user.svg';
import { MessagesContext } from 'Components/templates/Messages/MessagesSection';
// import { timeConversion } from 'Components/templates/Messages/timeConversion';

// import { Link } from 'react-router-dom';

// import styled from 'styled-components';

function defineUsername() {
  const companion = document.querySelector('.dialog-selected');
  if (companion) {
    return companion.id;
  }
  return '';
}

function messagesListOnOff() {
  document.querySelector('.messages-list').classList.toggle('active');
}

async function messageRequest(body, dest) {
  const res = await fetch(`http://devcom/api/message/${dest}`, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(body),
  });
  const message = await res.json();
  return message;
}

export function DialogWindow() {
  const { messages, setMessages, username } = useContext(MessagesContext);
  const messagesEndRef = useRef(null);

  useEffect(() => {
    const interval = setInterval(() => {
      checkForUpdates();
    }, 5000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    // console.log(messagesEndRef.current);
    scrollToBottom();
  }, [messages]);

  function scrollToBottom() {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView();
    }
  }

  function checkForUpdates() {
    if (messagesEndRef.current) {
      const id = messagesEndRef.current.id.slice(1, messagesEndRef.current.length);
      const username = document.querySelector('input[name=username]').value;
      messageRequest({ id, username }, 'updatechat').then(({ status, mes }) => {
        if (status === 'ok') {
          // document.querySelector('.dialog-window__form-input').value = '';
          if (mes) {
            setMessages((messages) => {
              return [...messages, ...mes];
            });
          }
        } else {
          console.log(mes);
        }
      });
    }
  }

  function addMessage(e) {
    e.preventDefault();
    const form = document.querySelector('.dialog-window__form');
    const data = Object.fromEntries(new FormData(form));
    messageRequest(data, 'send').then(({ status, mes }) => {
      if (status === 'ok') {
        document.querySelector('.dialog-window__form-input').value = '';
        setMessages([...messages, mes]);
      } else {
        console.log(mes);
      }
    });
  }

  console.log(messages);

  return (
    <div className="dialog-window">
      <div className="burger-menu" onClick={messagesListOnOff}>
        <span />
        <span />
        <span />
      </div>
      {messages[0] ? (
        messages[0].body ? (
          <ul className="dialog-window__messages-list">
            {messages.map((mes) => (
              <li
                key={mes.id.toString()}
                ref={messagesEndRef}
                className={`dialog-window__message${mes.isMine ? ' my' : ''}`}
                id={`m${mes.id}`}
              >
                <img className="dialog-window__user-photo" src={mes.profileimg} alt="usermes" />
                <div className="dialog-window__user-dialog-wrapper">
                  {!mes.isMine && <span className="dialog-window__message-text">{mes.body}</span>}
                  <span className="dialog-window__message-time">
                    {mes.created_at.slice(11, -3)}
                  </span>
                  {mes.isMine && <span className="dialog-window__message-text">{mes.body}</span>}
                </div>
              </li>
            ))}
          </ul>
        ) : (
          <span className="dialog-window__start-chat">Start chat with {username}</span>
        )
      ) : (
        <span className="dialog-window__start-chat">Select a chat to start messaging</span>
      )}
      {/* </div> */}
      <form className="dialog-window__form" action="" onSubmit={(e) => addMessage(e)}>
        <input
          className="dialog-window__form-input"
          type="text"
          placeholder="Write a message..."
          name="body"
        />
        <input
          type="text"
          name="username"
          defaultValue={defineUsername()}
          className="visually-hidden"
        />
        <input className="dialog-window__form-button" type="submit" value="Send" />
      </form>
    </div>
  );
}
