export function timeConversion(strTime) {
  const update = Math.floor((Date.now() - Date.parse(strTime)) / 60 / 1000);
  if (update < 60) {
    return `${update}m ago`;
  }
  if (Math.floor(update / 60) < 24) {
    return `${Math.floor(update / 60)}h ago`;
  }
  return `${Math.floor(update / 60 / 24)}d ago`;
}
