import React, { useEffect, useState } from 'react';
import { FriendsItem } from '../../index';
import './FriendsList.scss';
// import { Link } from 'react-router-dom';

// import styled from 'styled-components';

export function FollowersList() {
  return (
    <ul className="section-friends__friend-list">
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
      <FriendsItem />
    </ul>
  );
}
