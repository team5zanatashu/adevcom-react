import React, { useEffect, useState } from 'react';
import { useLocation, useHistory, useParams } from 'react-router';
// import { Link } from 'react-router-dom';

import './FriendsList.scss';
import { subscribeRequest } from 'Components/templates/FriendsPage/FriendsList/subscribeRequest';
import { FriendsItem } from '../../index';

export function FriendsList() {
  const [tabname, setTabname] = useState('followers');
  const [list, setList] = useState([]);
  const location = useLocation();
  const { username } = useParams();

  useEffect(() => {
    const tab = location.pathname.match(/(followers$|following$)/)[0];
    setTabname(tab);
  }, [location]);

  useEffect(() => {
    subscribeRequest('devcom', tabname, username).then(({ status, mes }) => {
      if (status === 'ok') {
        setList(mes);
      } else {
        console.log(mes);
      }
    });
  }, [tabname]);
  return (
    <section className="section-friends__main">
      <div className="section-friends__friends-count">
        <span>{`${username}'s ${tabname}`}</span>
      </div>
      <ul className="section-friends__friend-list">
        {list.map((item) => (
          <FriendsItem item={item} />
        ))}
      </ul>
      {/* <div className="section-account__column-one"> */}
      {/*  <div className="section-account__profile-intro">/!* <ProfileIntro /> *!/</div> */}
      {/* </div> */}
      {/* <div className="section-account__column-two"> */}
      {/*  <section className="section-account__main-column">/!* <MainColumn /> *!/</section> */}
      {/* </div> */}
      {/* <div className="section-account__column-three"> */}
      {/*  <section className="section-account__friends">/!* <Friends /> *!/</section> */}
      {/* </div> */}
    </section>
  );
}
