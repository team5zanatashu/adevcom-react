import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link, useHistory } from 'react-router-dom';

import './FriendsItem.scss';
import messageImg from 'Assets/icons/messenger.svg';
import friendsImg from 'Assets/icons/friends.svg';
import friendsActiveImg from 'Assets/icons/friends-active.svg';
import { subscribeRequest } from 'Components/templates/FriendsPage/FriendsList/subscribeRequest';

// import iconFriendPhoto from 'Assets/icons/user.svg';

// import styled from 'styled-components';
async function messageRequest(body, dest) {
  const res = await fetch(`http://devcom/api/message/${dest}`, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(body),
  });
  const message = await res.json();
  return message;
}

export function FriendsItem(props) {
  const { item } = props;
  const [isSubscribedImg, setIsSubscribed] = useState(friendsImg);
  const history = useHistory();

  useEffect(() => {
    const img = item.isMeSubscribed ? friendsActiveImg : friendsImg;
    setIsSubscribed(img);
  }, [item]);
  // console.log(item);

  function subscribeHandler(e) {
    e.preventDefault();
    const subscribeSubject = item.username;
    subscribeRequest('devcom', 'change', subscribeSubject).then(({ status, mes }) => {
      if (status === 'ok') {
        // localStorage.setItem('user', mes);
        const img = mes === 'subscribed' ? friendsActiveImg : friendsImg;
        setIsSubscribed(img);
        console.log(mes);
      } else {
        console.log(mes);
      }
    });
  }

  function createChat(e) {
    e.preventDefault();
    const data = { username: item.username, body: '' };
    messageRequest(data, 'send').then(({ status, mes }) => {
      if (status === 'ok') {
        history.push({
          pathname: '/user/messages',
          state: { username: item.username },
        });
      } else {
        console.log(mes);
      }
    });
  }

  return (
    <li key={`f${item.follower_id}`} className="section-friends__friend-item friend-item">
      <img className="friend-item__background" src={item.background} alt="background" />
      <div className="friend-item__top-wrapper">
        <img className="friend-item__image" src={item.profileimg} alt="Friends Logo" />
        <Link to={`/user/${item.username}`} className="friend-item__name">
          {item.firstname} {item.lastname}
        </Link>
        <Link to={`/user/${item.username}`} className="friend-item__username">
          @{item.username}
        </Link>
        {/* <span className="friend-item__username">@{item.username}</span> */}
      </div>
      <div className="friend-item__bottom-wrapper">
        <div>
          <span className="friend-item__follow--num">{item.followers}</span>
          <span className="friend-item__follow--text">Followers</span>
        </div>
        <div>
          <span className="friend-item__follow--num">{item.following}</span>
          <span className="friend-item__follow--text">Following</span>
        </div>
      </div>
      <div className="friend-item__interactions">
        {!item.isMe && (
          <>
            <button className="interaction-button" onClick={subscribeHandler}>
              <img src={isSubscribedImg} alt="friends" width="40" height="40" />
            </button>
            <button className="interaction-button" onClick={createChat}>
              <img src={messageImg} alt="friends" width="40" height="40" />
            </button>
          </>
        )}
      </div>
    </li>
  );
}

FriendsItem.propTypes = {
  item: PropTypes.object,
};
