export async function subscribeRequest(host, dest, username) {
  const body = { username };
  const res = await fetch(`http://${host}/api/subscribe/${dest}`, {
    method: 'POST',
    // headers: {
    //   'Content-Type': 'application/json',
    // },
    credentials: 'include',
    body: JSON.stringify(body),
  });
  const data = await res.json();
  return data;
}
