import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { AccountHeader, Header, LeftSidebar, RightSidebar, FriendsList } from '../index';
import './FriendsPage.scss';
import { getUserInfo } from '../../../redux/actions';

export function FriendsPage() {
  // const [username, setUsername] = useState('');
  const dispatch = useDispatch();
  const { username } = useParams();
  // const userInfo = useSelector((state) => state.user.userInfo);
  useEffect(() => {
    // setUsername(username);
    dispatch(getUserInfo(username));
  }, []);

  return (
    <div>
      <Header pageName="Friends" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <RightSidebar />
          <div className="account-main__section-account section-account">
            <AccountHeader />
          </div>
          <div>
            <FriendsList />
          </div>
        </div>
      </main>
    </div>
  );
}

FriendsPage.propTypes = {
  match: PropTypes.object,
  location: PropTypes.object,
};
