import React, { useState } from 'react';

export function UserCard() {
  return (
    <div className="user-card-wrapper">
      <div className="rounded bg-white user-card">
        <div className="rounded-top user-card__profile-bg" />
        <div className="user-card__profile-info temp">
          <img src="" className=" position-relative user-card__ava" />
          <div className="">
            <h4 className="card-title font-weight-light ">
              {/* <a href="{{ localhost }}user/account" */}
              {/*   className="text-dark user-card__user-name">{{ */}
              {/*    user.firstname */}
              {/* }} {{ */}
              {/*    user.lastname */}
              {/* }}</a> */}
            </h4>
            <p className="text-muted font-weight-bold font-size-14">
              {/* <a href="{{ localhost }}user/account" */}
              {/*   className="text-reset user-card__user-login">@{{ */}
              {/*    user */}
              {/*    .username */}
              {/* }}</a> */}
            </p>
          </div>
        </div>
        <div>
          <ul
            className="nav-twets-menu nav nav-tabs navbar-nav bg-white rounded-bottom"
            id="nav-tweets-tabs"
          >
            <li className="nav-item ">
              <a className="nav-link user-card__nav-link" id="column1" href="#tweets">
                tweets
              </a>
              <label htmlFor="column1" className="navbar-text-color">
                781
              </label>
            </li>
            <li className="nav-item activity-border ">
              <a className="nav-link user-card__nav-link" id="column3" href="#following">
                following
              </a>
              <label htmlFor="column3" className="navbar-text-color ">
                164
              </label>
            </li>
            <li className="nav-item activity-border ">
              <a
                className="nav-link px-0 pb-0 pt-0 border-0 user-card__nav-link"
                id="column4"
                href="#followers"
              >
                followers
              </a>
              <label htmlFor="column4" className="navbar-text-color ">
                277
              </label>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
