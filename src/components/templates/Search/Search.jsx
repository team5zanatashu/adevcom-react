// import React, { useState } from 'react';
// import { RightSidebar } from '../index';
//
// // import HeaderSearchList from '../../public/scripts/header-search-list';
// // import TabsClicker from '../../public/scripts/tabs-clicker';
//
// export function Search() {
//   return (
//     <div>
//       <section className="main-section main-bg-color">
//         {/* <Header /> */}
//         <div className="main-wrapper container">
//           <div className="row">
//             {/* <LeftSidebar /> */}
//             Left Sidebar
//             <div className="tweets-main">
//               <div className="tab-content">
//                 <div className="tweets-main__content" id="followers">
//                   <div className="search-text">
//                     <span>Search people for people on DevCommunication</span>
//                   </div>
//                 </div>
//                 This is Search
//                 <RightSidebar />
//               </div>
//             </div>
//           </div>
//         </div>
//       </section>
//     </div>
//   );
// }

// import React, { useState } from 'react';
// // import Header from './templates/Header';
// // import UserCard from './templates/UserCard';
// import { RightSidebar, MainColumn, LeftSidebar} from '../index'
// // import { Footer } from './templates/Footer';
//
// // import HeaderSearchList from '../../../public/scripts/header-search-list';
// // import TabsClicker from '../../../public/scripts/tabs-clicker';
// // import TweetScripts from '../../public/scripts/tweet-header-toggle-script';
// // import TweetHeaderToggleScript from '../../public/scripts/tweet-header-toggle-script';
// // import AddTweetValidation from '../../public/scripts/add-tweet-validation';
// // import PostActions from '../../public/scripts/post-actions';
//
// export function Homepage() {
//   return (
//     <section className=" main-section main-bg-color">
//       {/* <Header /> */}
//       <p>Header</p>
//       <div className="main-wrapper">
//         <div className="">
//           {/* <UserCard /> */}
//           <div className="tweets-main">
//             <div className="tab-content">
//               <LeftSidebar />
//               <MainColumn />
//               <RightSidebar />
//               <p>Homepage</p>
//             </div>
//             <div />
//             {/* <Footer /> */}
//             Footer
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// }

import React from 'react';
import {
  MainColumn,
  ProfileIntro,
  Friends,
  LeftSidebar,
  RightSidebar,
  Header,
  SearchResults,
} from '../index';
import './Search.scss';


export function Search() {
  document.title = 'Search';
  return (
    // {% extends "src/components/views/Default.jsx" %}
    <div>
      <Header pageName="Search Page" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <LeftSidebar />
          <RightSidebar />
          <div>
            <section className="section-search__main">
              <SearchResults />
              {/* <div className="section-account__column-one"> */}
              {/*  <div className="section-account__profile-intro">/!* <ProfileIntro /> *!/</div> */}
              {/* </div> */}
              {/* <div className="section-account__column-two"> */}
              {/*  <section className="section-account__search-column"> */}
              {/* <MainColumn /> */}

              {/* </section> */}
              {/* </div> */}
              {/* <div className="section-account__column-three"> */}
              {/*  <section className="section-account__friends">/!* <Friends /> *!/</section> */}
              {/* </div> */}
            </section>
          </div>
        </div>
      </main>
    </div>
  );
}
