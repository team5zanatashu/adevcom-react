import React, { useState } from 'react';
import './SearchResults.scss';
import { FriendsItem } from 'Components/templates';
import { useSelector } from 'react-redux';

export function SearchResults() {
  const users = useSelector((state) => state.user.searchUser);

  return (
    <div className="section-search__result">
      <div className="section-search__result-text--wrapper">
        <span className="section-search__result-text">
          Showing {users.length} Results for: “Front-End”
        </span>
      </div>
      <ul className="section-search__result-list">
        {Array.isArray(users)
          ? users.map((user) => <FriendsItem item={user} key={user.username} />)
          : ''}
      </ul>
    </div>
  );
}
