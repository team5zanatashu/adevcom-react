import React, { useState } from 'react';

export function Header() {
  return (
    <div className="Header">
      <header className="main-header page-header">
        <nav className="col-sm-12 col-xl-12 navbar header-main-nav header-navbar container-fluid navbar-expand-lg navbar-light bg-light px-4">
          <div className="burger-wrap">
            <div className="burger-menu-wrapper">
              <input type="checkbox" className="main-header-burg-button" value="menu" />
              <span className="burger-menu-slice" />
              <span className="burger-menu-slice" />
              <span className="burger-menu-slice" />
              <nav className="main-header-burger">
                <div className="burger-user-profile-block">
                  <img
                    className="img-thumbnail burger-menu-img mb-3"
                    src="../../public/img/Your-profile-pic-here.png"
                    alt=""
                  />
                  <h3>
                    <a
                      className="personal-info__user-name font-weight-light text-dark text-decoration-none"
                      href="user/account"
                    >
                      firstname
                    </a>
                  </h3>
                  <span className="personal-info__user-handle font-weight-bold font-size-14 d-block mb-2">
                    <a href="user/account" className="text-decoration-none text-muted">
                      @username
                    </a>
                  </span>
                  <ul
                    className="nav-twets-menu burger-tweets-menu nav nav-tabs navbar-nav text-uppercase d-flex flex-row flex-wrap my-4"
                    id="nav-tweets-tabs"
                  >
                    <li className="nav-item  activity-border nav-item d-flex flex-column">
                      <a className="nav-link px-0 pb-0 pt-0 border-0" id="column1" href="#tweets">
                        tweets
                      </a>
                      <label htmlFor="column1" className="navbar-text-color mb-1">
                        781
                      </label>
                    </li>
                    <li className="nav-item  activity-border nav-item d-flex flex-column">
                      <a
                        className="nav-link px-0 pb-0 pt-0 border-0"
                        id="column3"
                        href="#following"
                      >
                        following
                      </a>
                      <label htmlFor="column3" className="navbar-text-color mb-1">
                        164
                      </label>
                    </li>
                    <li className="nav-item   activity-border nav-item d-flex flex-column">
                      <a
                        className="nav-link px-0 pb-0 pt-0 border-0"
                        id="column4"
                        href="#followers"
                      >
                        followers
                      </a>
                      <label htmlFor="column4" className="navbar-text-color mb-1">
                        277
                      </label>
                    </li>
                  </ul>
                </div>
                <ul className="burger-menu-link-list d-flex flex-column">
                  <li className="burger-link-list__item">
                    <a
                      className="burger-link text-decoration-none text-muted font-weight-bold"
                      href="user/homepage"
                    >
                      Home
                    </a>
                  </li>
                  <li className="burger-link-list__item">
                    <a
                      className="burger-link text-decoration-none text-muted font-weight-bold"
                      href="user/search"
                    >
                      Search
                    </a>
                  </li>
                  <li className="burger-link-list__item">
                    <a
                      className="burger-link text-decoration-none text-muted font-weight-bold"
                      href="user/logout"
                    >
                      Logout
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>

          <nav
            className="collapse col-xl-12 navbar-collapse header-main-navigation"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav text-muted main-header-menu-list px-2">
              <li className="d-flex flex-nowrap align-items-center ml-3 main-header-menu-item justify-content-center">
                <a className="mr-1" href="user/homepage" id="home">
                  <img src="../../public/img/icons/Home_icon.png" alt="home" />
                </a>
                <label className="mb-0" htmlFor="home">
                  <a className="text-decoration-none custom-a text-muted" href="user/homepage">
                    Home
                  </a>
                </label>
              </li>
            </ul>
            <div className="mx-auto main-header-nav-logo">
              <a className="navbar-brand mx-auto" href="{{ localhost }}user/homepage">
                <img src="../../public/img/icons/Twitter_logo.png" alt="logo" />
              </a>
            </div>
            <ul className="navbar-nav text-muted justify-content-between list-width flex-row">
              <li className="d-flex flex-nowrap custom-search-field align-items-center px-2">
                <input
                  className="w-100 search-if text-light"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <input
                  type="image"
                  src="../../public/img/icons/search_icon@2x.png"
                  className="custom-search-logo"
                />
                <ul className="header-search-list" />
              </li>
              <li className="d-flex flex-nowrap ml-3 align-items-center main-header-menu-icon">
                <div className="p-1 custom-input-btms d-flex align-items-center mx-auto">
                  <input
                    type="image"
                    src="../../public/img/icons/Compose_icon.svg"
                    className=""
                    alt="compose_icon"
                  />
                </div>
              </li>
              <li className="d-flex flex-nowrap ml-3 align-items-center main-header-menu-icon">
                <a
                  href="user/logout"
                  className="p-1 d-flex align-items-center mx-auto text-muted font-weight-bold text-decoration-none"
                >
                  Logout
                </a>
              </li>
            </ul>
          </nav>
        </nav>
      </header>
    </div>
  );
}
