// import React, { useState } from 'react';
// // import Header from './templates/Header';
// // import UserCard from './templates/UserCard';
// import { RightSidebar, MainColumn, LeftSidebar} from '../index'
// // import { Footer } from './templates/Footer';
//
// // import HeaderSearchList from '../../../public/scripts/header-search-list';
// // import TabsClicker from '../../../public/scripts/tabs-clicker';
// // import TweetScripts from '../../public/scripts/tweet-header-toggle-script';
// // import TweetHeaderToggleScript from '../../public/scripts/tweet-header-toggle-script';
// // import AddTweetValidation from '../../public/scripts/add-tweet-validation';
// // import PostActions from '../../public/scripts/post-actions';
//
// export function Homepage() {
//   return (
//     <section className=" main-section main-bg-color">
//       {/* <Header /> */}
//       <p>Header</p>
//       <div className="main-wrapper">
//         <div className="">
//           {/* <UserCard /> */}
//           <div className="tweets-logout">
//             <div className="tab-content">
//               <LeftSidebar />
//               <MainColumn />
//               <RightSidebar />
//               <p>Homepage</p>
//             </div>
//             <div />
//             {/* <Footer /> */}
//             Footer
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// }

import React from 'react';
import { Link } from 'react-router-dom';
import {
  MainColumn,
  ProfileIntro,
  Friends,
  LeftSidebar,
  RightSidebar,
  Header,
  AccountHeader,
} from '../index';
import './Logout.scss';

export function Logout() {
  return (
    // {% extends "src/components/views/Default.jsx" %}
    <div>
      <Header pageName="Logged Out" />
      <main className="page-main">
        <div className="page-main__wrapper">
          <div className="account-main__section-account section-account">
            <AccountHeader />
          </div>
          <div>
            <section className="section-account__main">
              <div className="logout-text">
                <span className="logout-text__text">Do you wanna check Users Profile?</span>
                <span className="logout-text__action">
                  <Link className="logout-text__link" to="/user/login">Login</Link> or <Link className="logout-text__link" to="/user/login">Register</Link> now to create your own profile and have access to all the Devcommunication awesome features!</span>
              </div>
            </section>
          </div>
        </div>
      </main>
    </div>
  );
}
