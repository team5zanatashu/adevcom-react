import React, { useState } from 'react';
import { Header, LoginForm, SignupForm } from '../index';

import './Login.scss';

// import MenuScript from '../../public/scripts/menu-script';

export function Login() {
  const [form, setForm] = useState('login');

  function rippleEffect(e) {
    const btnParent = document.querySelector('.login-container');
    const x = e.clientX - 300 / 2 - (btnParent.offsetLeft + e.target.offsetLeft);
    const y = e.clientY - 300 / 2 - (btnParent.offsetTop + e.target.offsetTop);

    const ripple = document.createElement('span');
    ripple.classList.add('switcher-item__ripple');
    ripple.style.position = 'absolute';
    ripple.style.left = `${x}px`;
    ripple.style.top = `${y}px`;
    e.target.appendChild(ripple);
    setTimeout(() => ripple.remove(), 1000);
  }

  const formSwitcher = (e, formName) => {
    setForm(formName);

    e.target.classList.add('active');
    if (e.target.nextElementSibling) {
      e.target.nextElementSibling.classList.remove('active');
    } else if (e.target.previousElementSibling) {
      e.target.previousElementSibling.classList.remove('active');
    }
  };
  document.title = 'Dev Communication';
  return (
    <div className="login-wrapper">
      <Header pageName="welcome" />
      <div className="video-background">
        <div className="video-foreground">
          <iframe
            src="https://www.youtube.com/embed/fCPEAq6FMF8?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=fCPEAq6FMF8"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
      </div>

      <div className="login-inner-wrapper">
        <div className="startpage-text-wrapper">
          <span className="startpage-text">
            Welcome to the Smallest Social Network in the World
          </span>
        </div>

        <div className="login-container">
          <div className="form-switcher">
            <span
              onMouseDown={(e) => rippleEffect(e)}
              onMouseUp={(e) => formSwitcher(e, 'login')}
              className="switcher-item active"
            >
              <span className="switcher-item__inner">sign in</span>
            </span>
            <span
              onMouseDown={(e) => rippleEffect(e)}
              onMouseUp={(e) => formSwitcher(e, 'signup')}
              className="switcher-item"
            >
              <span className="switcher-item__inner">sign up</span>
            </span>
          </div>
          {form === 'login' && <LoginForm />}
          {form === 'signup' && <SignupForm />}
        </div>
      </div>
    </div>
  );
}
