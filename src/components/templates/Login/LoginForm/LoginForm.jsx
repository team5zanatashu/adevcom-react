import React, { useState } from 'react';
import { useHistory } from 'react-router';

import { LoginFormInput } from './LoginFormInput';
import './LoginForm.scss';
import { sendForm } from './sendForm';
// import { useDispatch, useSelector } from 'react-redux';
// import { getDefaultUser, getUserInfo } from '../../../../redux/actions';

export function LoginForm() {
  // const dispatch = useDispatch();
  // const [user, setUser] = useState('');

  const [title, setTitle] = useState('Login to your Account');
  const history = useHistory();
  function tryLogin(e) {
    e.preventDefault();
    let hasErrors = false;
    const regErrors = document.querySelectorAll('.reg-errors');
    regErrors.forEach((err) => {
      if (err.textContent) {
        hasErrors = true;
      }
    });
    if (!hasErrors) {
      sendForm('devcom', 'login').then(({ status, mes }) => {
        if (status === 'ok') {
          document.cookie = `username=${mes}; max-age=3600`;
          // window.location.href = `${window.location.origin}/user/${mes}`;
          // dispatch(getDefaultUser(mes));

          history.push(`/user/${mes}`);
        } else if (status === 'not found') {
          document.querySelector('.login-form__title').classList.add('err');
          setTitle(mes);
        } else {
          console.log(mes);
        }
      });
    }
  }

  return (
    <div className="login-form">
      <h3 className="login-form__title">{title}</h3>
      <form action="" method="POST" className="login-form__login">
        <div className="login-form__body">
          <LoginFormInput name="email" />
          <LoginFormInput name="password" />
          <button type="submit" className="login-form__input login-form__button" onClick={tryLogin}>
            Login
          </button>
        </div>
      </form>
    </div>
  );
}
