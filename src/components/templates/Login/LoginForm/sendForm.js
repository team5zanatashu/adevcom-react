export async function sendForm(host, dest) {
  const form = document.querySelector(`.login-form__${dest}`);
  const formData = Object.fromEntries(new FormData(form));
  const res = await fetch(`http://${host}/api/user/${dest}`, {
    method: 'POST',
    // headers: {
    //   'Content-Type': 'application/json',
    //   // 'Set-Cookie': 'test=testCookie',
    // },
    credentials: 'include',
    body: JSON.stringify(formData),
  });
  const data = await res.json();
  // const headers = await res.headers.values();
  // console.log(headers)
  return data;

  // window.location.href = `${window.location.origin}/user/account`;
}
