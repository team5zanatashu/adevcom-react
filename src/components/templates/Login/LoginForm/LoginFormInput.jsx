import React, { useState } from 'react';
import PropTypes from 'prop-types';
// import MenuScript from '../../public/scripts/menu-script';
// import './LoginForm.scss';
import {
  validateFirstname,
  validateLastname,
  validateUsername,
  validateEmail,
  validatePassword,
  validateConfirm,
} from './validation';

export function LoginFormInput(props) {
  const { name, children } = props;
  const [err, setErr] = useState('');
  function validateInput(e) {
    const password = document.querySelector('input[name=password]').value;
    let err;
    switch (name) {
      case 'firstname':
        err = validateFirstname(e.target.value);
        break;
      case 'lastname':
        err = validateLastname(e.target.value);
        break;
      case 'username':
        err = validateUsername(e.target.value);
        break;
      case 'email':
        err = validateEmail(e.target.value);
        break;
      case 'password':
        err = validatePassword(e.target.value);
        break;
      case 'confirm':
        err = validateConfirm(e.target.value, password);
        break;
    }
    setErr(err);
    if (err) {
      e.target.classList.remove('valid');
      e.target.classList.add('invalid');
    } else {
      e.target.classList.remove('invalid');
      e.target.classList.add('valid');
    }
  }
  return (
    <div>
      <input
        type={name === 'password' || name === 'confirm' ? 'password' : 'text'}
        name={name}
        // value="{{ formdata.email }}"
        className={`login-form__input login-form__${name}`}
        placeholder={
          name === 'confirm'
            ? 'Confirm password'
            : name[0].toUpperCase() + name.slice(1, name.length)
        }
        onBlur={validateInput}
      />
      {!err && children}
      <p className="reg-errors">{err}</p>
    </div>
  );
}

LoginFormInput.propTypes = {
  name: PropTypes.string,
};
