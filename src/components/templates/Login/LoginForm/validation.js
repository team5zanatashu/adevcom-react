export function validateFirstname(str) {
  const reg = /^[a-zA-Zа-яА-Я-]{3,29}[a-zA-Zа-яА-Я]$/;
  if (str.length === 0) {
    return 'Field is empty. Please fill it.';
  }
  if (str.length < 3) {
    return 'Too short firstname. Firstname should have at least three characters.';
  }
  if (str.length > 30) {
    return "Too long firstname. Firstname shouln'd have more than thirty characters.";
  }
  if (!reg.test(str)) {
    return 'Invalid firstname. Firstname can\'t contain symbols (except "-"), numbers or spaces';
  }

  return '';
}

export function validateLastname(str) {
  const reg = /^[a-zA-Zа-яА-Я-]{3,29}[a-zA-Zа-яА-Я]$/;
  if (str.length === 0) {
    return 'Field is empty. Please fill it.';
  }
  if (str.length < 3) {
    return 'Too short lastname. Lastname should have at least three characters.';
  }
  if (str.length > 30) {
    return "Too long lastname. Lastname shouln'd have more than thirty characters.";
  }
  if (!reg.test(str)) {
    return 'Invalid lastname. Lastname can\'t contain symbols (except "-"), numbers or spaces';
  }
  return '';
}

export function validateUsername(str) {
  const reg = /^\w{3,15}$/;
  if (str.length === 0) {
    return 'Field is empty. Please fill it.';
  }

  if (str.length < 3) {
    return 'Too short username. Username should have at least three characters.';
  }
  if (str.length > 15) {
    return "Too long username. Username shouln'd have more than fifteen characters.";
  }
  if (!reg.test(str)) {
    return 'Invalid username. Username can\'t contain symbols (except "_"), numbers or spaces';
  }
  return '';
}

export function validateEmail(str) {
  const reg = /^[a-zA-Z]+\d*[a-zA-Z]+\d*@[a-z]+\.[a-z]{2,3}$/;
  if (str.length === 0) {
    return 'Field is empty. Please fill it.';
  }
  if (!reg.test(str)) {
    return 'Invalid email. Email can\'t contain symbols (except "@") or spaces';
  }
  return '';
}

export function validatePassword(str) {
  const reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z]{8,}$/;
  if (str.length === 0) {
    return 'Field is empty. Please fill it.';
  }

  if (str.length < 8) {
    return 'Too short password. Password should have at least eight characters.';
  }
  if (!reg.test(str)) {
    return 'Invalid password. Password must contain one uppercase and number';
  }
  return '';
}

export function validateConfirm(str, password) {
  // const validate = validatePassword(str);
  // if (validate) {
  //   return validate;
  // }
  if (str !== password) {
    return 'Password does not match.';
  }
  return '';
}
