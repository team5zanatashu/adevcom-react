import React, { useState } from 'react';

import { LoginFormInput } from './LoginFormInput';
import './LoginForm.scss';
import { SignupSelect } from '../SignupSelect/SignupSelect';
import { sendForm } from './sendForm';
// import {getDefaultUser} from "../../../../redux/actions";
// import {useDispatch} from "react-redux";

export function SignupForm() {
  // const dispatch = useDispatch();
  const [title, setTitle] = useState('Register to DevCommunication');
  function trySignup(e) {
    e.preventDefault();
    let hasErrors = false;
    const regErrors = document.querySelectorAll('.reg-errors');
    regErrors.forEach((err) => {
      if (err.textContent) {
        hasErrors = true;
      }
    });
    if (!hasErrors) {
      sendForm('devcom', 'signup').then(({ status, mes }) => {
        if (status === 'ok') {
          document.cookie = `username=${mes}; max-age=3600`;
          // dispatch(getDefaultUser(mes));
          window.location.href = `${window.location.origin}/user/${mes}`;
        } else if (status === 'exists') {
          document.querySelector('.login-form__title').classList.add('err');
          if (mes.email) {
            document.querySelector('input[name=email]').classList.add('invalid');
          }
          if (mes.username) {
            document.querySelector('input[name=username]').classList.add('invalid');
          }
          setTitle(mes.error);
        } else {
          console.log(mes);
        }
      });
    }
  }
  return (
    <div className="login-form">
      <h3 className="login-form__title">{title}</h3>
      <form action="" method="POST" className="login-form__signup">
        <div className="login-form__body">
          <LoginFormInput name="firstname" />
          <LoginFormInput name="lastname" />
          <LoginFormInput name="email" />
          <LoginFormInput name="username">
            <p className="hints">
              Username must be at least three(3) characters long and must contain one underscore and
              number
            </p>
          </LoginFormInput>
          <div className="date-of-birth">
            <span className="dob-text">Date of birth:</span>
            <div className="select-dob-wrapper">
              <SignupSelect option="month" />
              <SignupSelect option="day" />
              <SignupSelect option="year" />
            </div>
          </div>
          <LoginFormInput name="password">
            <p className="hints">
              Password must be at least eight(8) chapters long and must contain one uppercase and
              number
            </p>
          </LoginFormInput>
          <LoginFormInput name="confirm" />
          <button
            type="submit"
            className="login-form__input login-form__button"
            onClick={trySignup}
          >
            Create account
          </button>
        </div>
      </form>
    </div>
  );
}
