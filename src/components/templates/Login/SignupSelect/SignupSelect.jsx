import React, { useState } from 'react';
import PropTypes from 'prop-types';

const months = [];
const days = [];
const years = [];
const currentYear = Math.floor(Date.now() / 1000 / 3600 / 24 / 365);

for (let i = 1; i <= currentYear + 20; i++) {
  if (i <= 9) {
    months.push(`0${i}`);
    days.push(`0${i}`);
    years.push(`${currentYear + 1970 - i}`);
  } else if (i <= 12) {
    months.push(`${i}`);
    days.push(`${i}`);
    years.push(`${currentYear + 1970 - i}`);
  } else if (i <= 31) {
    days.push(`${i}`);
    years.push(`${currentYear + 1970 - i}`);
  } else {
    years.push(`${currentYear + 1970 - i}`);
  }
}

export function SignupSelect(props) {
  const { option } = props;
  let optsArr;
  switch (option) {
    case 'month':
      optsArr = months;
      break;
    case 'day':
      optsArr = days;
      break;
    case 'year':
      optsArr = years;
      break;
  }
  function selectedHandler(e) {
    const optionsContainer = e.target.previousElementSibling;
    optionsContainer.classList.toggle('active');
  }
  function optionHandler(e) {
    let selectedOpt;
    const options = document.querySelectorAll('.signup-radio');
    for (let i = 0; i < options.length; i++) {
      if (options[i] === e.target || options[i] === e.target.firstElementChild) {
        selectedOpt = options[i].parentNode;
        break;
      }
    }
    if (!selectedOpt) {
      return;
    }
    const optionsContainer = selectedOpt.parentNode;
    const selected = optionsContainer.nextElementSibling;
    selected.textContent = selectedOpt.querySelector('label').textContent;
    optionsContainer.classList.remove('active');
    const checkedInp = optionsContainer.querySelector('input[checked]');
    if (checkedInp) {
      checkedInp.toggleAttribute('checked');
    }
    selectedOpt.firstElementChild.toggleAttribute('checked');
  }
  return (
    <div className="select-box">
      <div className="options-container">
        <div key={`${option}`} className="option">
          <input
            type="radio"
            className="signup-radio"
            key={`${option}`}
            id={`${option}`}
            name={option}
            value={option}
            defaultChecked
          />
          <label htmlFor={`${option}`}>{option}</label>
        </div>
        {optsArr.map((item) => (
          <div key={`${option}${item}`} className="option" onClick={(e) => optionHandler(e)}>
            <input
              type="radio"
              className="signup-radio"
              key={`${option}${item}`}
              id={`${option}${item}`}
              name={option}
              value={item}
              // defaultChecked={item === option}
            />
            <label htmlFor={`${option}${item}`}>{item}</label>
          </div>
        ))}
      </div>
      <div className="selected" onClick={(e) => selectedHandler(e)}>
        {option}
      </div>
    </div>
  );
}

SignupSelect.propTypes = {
  option: PropTypes.string,
};
