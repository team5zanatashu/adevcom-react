import React, { useState } from 'react';

export function MinUserCard() {
  return (
    <div>
      <div className="user-card-wrapper">
        <div className="rounded user-card">
          <div className="rounded-top user-card__profile-bg">Profile img</div>
          <div className="user-card__profile-info temp">
            <img src="" className="position-relative user-card__ava" />
            <div className="">
              <h4 className="card-title font-weight-light ">
                <a href="{{ localhost }}user/account" className=" user-card__user-name">
                  user name
                </a>
              </h4>
              <p className="">
                <a href="#">User Name</a>
              </p>
            </div>
          </div>
          <div className="user-card__actions">Actions</div>
        </div>
      </div>
    </div>
  );
}
