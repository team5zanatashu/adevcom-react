import React, { useState } from 'react';

export function Error() {
  return (
    <div
      className="justify-content-center err404-wrapper"
      style="height: 200px; background-color: black"
    >
      <div className="col-12 pl-5" style="position: absolute; top: 20px; z-index: 1000">
        <h1 style="color: white; font-family: sans-serif;">404</h1>
        <h3 style="color: white; font-family: sans-serif; margin-bottom: 20px">
          The page you look for is not found or deleted :(
        </h3>
        <a className="btn btn-secondary btn-lg" href="../../../errors/comeback" role="button">
          Come back
        </a>
      </div>
      <video
        autoPlay
        muted
        controls
        className="video-err404"
        width="100%"
        style=";position: absolute; top: -50px; z-index: 999; outline: none"
      />
    </div>
  );
}
