const path = require('path');
const fs = require('fs');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

// console.log('NODE_ENV', process.env.NODE_ENV)

const ROOT_DIR = fs.realpathSync(process.cwd());
const analyze = process.argv.includes('--analyze'); // в массиве находится весь список ключей при запуске по типу --analyze

function pathResolve(...args) {
  return path.resolve(ROOT_DIR, ...args);
}
const isDev = process.env.NODE_ENV !== 'production'; // idDev ? ['./src/index.jsx', 'hot-reload.js'] :

function getName(ext) {
  return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`;
}

module.exports = {
  mode: isDev ? 'development' : 'production',
  entry: {
    main: './src/index.jsx',
  },
  output: {
    filename: getName('js'),
    path: pathResolve('dist'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx'], // расширения файлов, которые может обработать вебпак
    alias: {
      Components: pathResolve('src/components/'),
      Assets: pathResolve('src/assets/'),
      Styles: pathResolve('src/styles/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/],
        use: ['babel-loader'],
      },
      {
        test: /\.(css)$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `assets/icons/${getName('[ext]')}`,
            },
          },
        ],
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        use: ['file-loader'],
      },
      // {
      //     test: /\.svg$/,
      //     loader: 'svg-inline-loader'
      // }
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    // new HtmlWebpackPlugin( {
    //     filename: "login.html",
    //     template: pathResolve('public/login.html'),
    //     excludeChunks: ['main']
    //
    // }),
    new HtmlWebpackPlugin({
      template: pathResolve('public/index.html'),
      // excludeChunks: ['logger']
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
    analyze && new BundleAnalyzerPlugin(), // при build:dev этот вызов превратился в false
  ].filter(Boolean), // проверка на булево значение
  devServer: {
    port: '8081',
    open: true,
    historyApiFallback: true,
  },
};
